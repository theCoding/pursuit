php bin/console doctrine:schema:drop -n -q --force --full-database
php bin/console make:migration
php bin/console doctrine:migrations:migrate
rm src/Migrations/*.php
