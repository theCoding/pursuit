<?php

namespace App\Repository;

use App\Entity\Wareneingang;
use App\Repository\Traites\FindTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wareneingang|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wareneingang|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wareneingang[]    findAll()
 * @method Wareneingang[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WareneingangRepository extends ServiceEntityRepository
{
    use FindTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wareneingang::class);
    }


    // /**
    //  * @return Wareneingang[] Returns an array of Wareneingang objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wareneingang
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
