<?php

namespace App\Repository;

use App\Entity\Ansatz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ansatz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ansatz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ansatz[]    findAll()
 * @method Ansatz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnsatzRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ansatz::class);
    }

    // /**
    //  * @return Ansatz[] Returns an array of Ansatz objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ansatz
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
