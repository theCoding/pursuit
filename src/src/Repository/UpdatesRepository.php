<?php

namespace App\Repository;

use App\Entity\Updates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;    //Original
//use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Updates|null find($id, $lockMode = null, $lockVersion = null)
 * @method Updates|null findOneBy(array $criteria, array $orderBy = null)
 * @method Updates[]    findAll()
 * @method Updates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpdatesRepository extends ServiceEntityRepository
{
    //public function __construct(RegistryInterface $registry)
    public function __construct(ManagerRegistry $registry)  //Original
    {
        parent::__construct($registry, Updates::class);
    }

    // /**
    //  * @return Updates[] Returns an array of Updates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Updates
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
