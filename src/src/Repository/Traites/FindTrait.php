<?php

namespace App\Repository\Traites;
use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Einheit;

trait FindTrait {
//    const
//        STATE_BLOCKED = 1,
//        STATE_ENABLED = 2,
//        STATE_ARCHIVED = 3,
//        STATE_ERROR = 4,
//        STATE_ADM_LOCKED = 5;

    /**
     * @param bool $archive
     *
     * @return mixed
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function findAllWithArchive(bool $archive){
        $builder =  $this->createQueryBuilder('t')
            ->indexBy('t','t.id')
            ->where('t.ObjectState = :state');

        if(!$archive){
            $builder->setParameter('state', AbstractEntity::STATE_ENABLED);
        }else{
            $builder->setParameter('state', AbstractEntity::STATE_ARCHIVED);
        }
        $builder->orderBy('t.DateAdded');

        return  $builder->getQuery()->getResult();
    }

    /**
     * @param bool $archive
     *
     * @return mixed
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function findAllWithArchiveWithoutOrder(bool $archive){
        $builder =  $this->createQueryBuilder('t')
            ->indexBy('t','t.id')
            ->where('t.ObjectState = :state');

        if(!$archive){
            $builder->setParameter('state', AbstractEntity::STATE_ENABLED);
        }else{
            $builder->setParameter('state', AbstractEntity::STATE_ARCHIVED);
        }

        return  $builder->getQuery()->getResult();
    }


    /**
     * @return mixed
     */
    public function findAllKeyById(){
        $builder =  $this->createQueryBuilder('t')
            ->indexBy('t','t.id');


        return  $builder->getQuery()->getResult();
    }

    public function getCountEnabled(){
        $builder =  $this->createQueryBuilder('t')
            ->where('t.ObjectState = :state');
        $builder->setParameter('state', AbstractEntity::STATE_ENABLED);

        return  $builder->getQuery()->getScalarResult();
    }


    public function getAllForDateRange(string $dateStart, string $dateStop){
        $builder =  $this->createQueryBuilder('t');

        $builder->add('where', $builder->expr()->between(
            't.DateAdded',
            ':dateStart',
            ':dateStop'
        ));
        $builder->setParameter('dateStart' , $dateStart);
        $builder->setParameter('dateStop', $dateStop);

        $builder->andWhere('t.ObjectState = :state');
        $builder->setParameter('state', AbstractEntity::STATE_ENABLED);
//        var_dump($dateStart);
//        var_dump($dateStop);
//        var_dump($builder->getQuery()->getSql());exit;
        return  $builder->getQuery()->getResult();
    }

}