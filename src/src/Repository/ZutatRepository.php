<?php

namespace App\Repository;

use App\Entity\Einheit;
use App\Entity\Zutat;
use App\Repository\Traites\FindTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Zutat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zutat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zutat[]    findAll()
 * @method Zutat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZutatRepository extends ServiceEntityRepository
{
    use FindTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zutat::class);
    }


    /**
     * @param Einheit $einheit
     *
     * @return mixed
     */
    public function getZutatsForEinheit(object $einheit){
        $name = $einheit->getName();
        $builder =  $this->createQueryBuilder('t')
            ->where('t.Einheit = :einheit')
            ->setParameter('einheit', $name);

        return  $builder->getQuery()->getResult();
    }

    /**
     * @param string $einheitName
     *
     * @return mixed
     */
    public function getZutatsForEinheitName(string $einheitName){
        $builder =  $this->createQueryBuilder('t')
            ->where('t.Einheit = :einheit')
            ->setParameter('einheit', $einheitName);

        return  $builder->getQuery()->getResult();
    }
    // /**
    //  * @return Zutat[] Returns an array of Zutat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Zutat
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
