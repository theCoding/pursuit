<?php

namespace App\Repository;

use App\Entity\Warengruppe;
use App\Repository\Traites\FindTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Warengruppe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Warengruppe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Warengruppe[]    findAll()
 * @method Warengruppe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WarengruppeRepository extends ServiceEntityRepository
{
    use FindTrait;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Warengruppe::class);
    }

    // /**
    //  * @return Warengruppe[] Returns an array of Warengruppe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Warengruppe
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
