<?php

namespace App\Repository;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Lieferant;
use App\Repository\Traites\FindTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lieferant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lieferant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lieferant[]    findAll()
 * @method Lieferant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LieferantRepository extends ServiceEntityRepository
{
    use FindTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lieferant::class);
    }

    // /**
    //  * @return Lieferant[] Returns an array of Lieferant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lieferant
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function getAllForHerstellerOrLieferant(int $herstellerOrLieferant){
        $builder =  $this->createQueryBuilder('t');
        $builder->where('t.HerstellerOrLieferant = :value',);
        $builder->andWhere('t.ObjectState = :state',);
        $builder->setParameter('value', $herstellerOrLieferant);
        $builder->setParameter('state', Lieferant::STATE_ENABLED);

        return  $builder->getQuery()->getResult();
    }
}
