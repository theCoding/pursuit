<?php

namespace App\Repository;

use App\Entity\Rezeptur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Rezeptur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rezeptur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rezeptur[]    findAll()
 * @method Rezeptur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RezepturRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rezeptur::class);
    }

    // /**
    //  * @return Rezeptur[] Returns an array of Rezeptur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rezeptur
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
