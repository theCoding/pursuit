<?php

namespace App\Repository;

use App\Entity\Einheit;
use App\Repository\Traites\FindTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Einheit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Einheit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Einheit[]    findAll()
 * @method Einheit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EinheitRepository extends ServiceEntityRepository
{

    use FindTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Einheit::class);
    }




    // /**
    //  * @return Einheit[] Returns an array of Einheit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Einheit
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
