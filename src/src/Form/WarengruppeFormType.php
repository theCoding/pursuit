<?php

namespace App\Form;

use App\Entity\Warengruppe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class WarengruppeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name')
            /*->add('GruppeInt', ChoiceType::class, [
                'choices'  => $options['warengruppe'],
                'label' => 'Übergruppe',
            ])*/
            ->add('GruppeInt', ChoiceType::class, [
                'choices'  => $options['warengruppe'],
                'label' => 'Übergruppe',
                'attr'=> array('class'=> 'select2'),
                'multiple' => true,
                'required'   => false
            ])
            
            ->add('save', SubmitType::class)
            //->add('Uebergruppe')
            //->add('warengruppes')
            //->add('zutaten')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Warengruppe::class,
        ])
       
        ->setDefault('warengruppe', null)
        ->setRequired('warengruppe')
        ->setAllowedTypes('warengruppe', array('array'))
        ;
    }
}
