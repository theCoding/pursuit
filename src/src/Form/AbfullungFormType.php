<?php

namespace App\Form;

use App\Entity\Abfullung;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AbfullungFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Verbrauch_Ansatz')
            ->add('Menge_flaschen')
            ->add('Flaschen')
            ->add('LOS')
            //->add('Date')
            //->add('Ansatz')
            ->add('AnsatzInt', ChoiceType::class, [
                'choices'  => $options['ansatz'],
                'label' => 'Ansatz',
            ])
            //->add('Produkt')
            ->add('ProduktInt', ChoiceType::class, [
                'choices'  => $options['produkt'],
                'label' => 'Produkt',
            ])
            //->add('User_added')
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Abfullung::class,
        ])
        ->setDefault('produkt', null)
        ->setRequired('produkt')
        ->setAllowedTypes('produkt', array('array'))
        ->setDefault('ansatz', null)
        ->setRequired('ansatz')
        ->setAllowedTypes('ansatz', array('array'))
        ;
    }
}
