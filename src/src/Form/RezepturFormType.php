<?php

namespace App\Form;

use App\Entity\Rezeptur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class RezepturFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Bezeichnung')
            ->add('Beschreibung')
            ->add('Notiz')
            ->add('Alkoholgehalt')
            //->add('date_added')
            //->add('Zutaten')
            //->add('Menge')
            //->add('Lieferant')
            //->add('User_added')
            ->add('ZutatString', HiddenType::class, [
                'data' => 'abcdef',
                'attr' => ['class' => 'pick_zutat'],
            ])
            ->add('Zutaten', CollectionType::class, [
                'entry_type' => TagType::class,
                'entry_options' => ['label' => false],
            ])
            //->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rezeptur::class,
        ])
       /* ->setDefault('lieferanten', null)
        ->setAllowedTypes('lieferanten', array('array'))
        ->setDefault('warengruppe', null)
        ->setAllowedTypes('warengruppe', array('array'))
        ->setDefault('zutaten', null)
        ->setAllowedTypes('zutaten', array('array'))*/
        ;
    }
}
