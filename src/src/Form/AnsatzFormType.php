<?php

namespace App\Form;

use App\Entity\Ansatz;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AnsatzFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           //->add('Datum')
           // ->add('Status')
            ->add('Status', ChoiceType::class, [
                'choices'  => ["Leer" => 0, "Fertiggestellt und Abfüllbereit" => 1, "In zubereitung" => 2, "geplant" => 3] ,
                'label' => 'Status',
            ])
            //->add('User')
            //->add('Produkt')
            ->add('ProduktInt', ChoiceType::class, [
                'choices'  => $options['rezept'],
                'label' => 'Rezept',
            ])
            ->add('Menge')
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ansatz::class,
        ])
        ->setDefault('rezept', null)
        ->setRequired('rezept')
        ->setAllowedTypes('rezept', array('array'))
        ;
    }
}
