<?php

namespace App\Form;

use App\Entity\Produkt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProduktFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Bezeichnung')
            //->add('Date_added')
            ->add('Kunde')
            ->add('RezeptInt', ChoiceType::class, [
                'choices'  => $options['rezepte'],
                'label' => 'Rezept',
            ])
            //->add('Rezept')
           // ->add('User_added')
           ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produkt::class,
        ])
        ->setDefault('rezepte', null)
        ->setRequired('rezepte')
        ->setAllowedTypes('rezepte', array('array'))
        ;
    }
}
