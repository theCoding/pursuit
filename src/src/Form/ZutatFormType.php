<?php

namespace App\Form;

use App\Entity\Zutat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Lieferant;
use App\Entity\Warengruppe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class ZutatFormType extends AbstractType
{
    
    public function lieferantenGetter(){

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $lieferanten =$options['lieferanten'];
        $einheit =$options['einheit'];
        $warengruppe =$options['warengruppe'];


        $builder->add('Einheit', ChoiceType::class, [
            'choices'  => $einheit,
            'label' => 'Einheit',
            'attr'=> array('class'=> 'select2'),
            'multiple' => false
        ])
            ->add('Name')
            ->add('Notiz', null, ['required'   => false,])

            ->add('LieferInt', ChoiceType::class, [
                'choices'  => $lieferanten,
                'label' => 'Lieferant(optional)',
                'attr'=> array('class'=> 'allowEmptySelect2', ),
                'multiple' => true,
                'required' => false,
                'empty_data' => null,
            ])
            ->add('GruppeInt', ChoiceType::class, [
                'choices'  => $warengruppe,
                'label' => 'Warengruppe(optional',
                'attr'=> array('class'=> 'allowEmptySelect2'),
                'multiple' => true,
                'required'   => false,
                'empty_data' => null,
            ])

            ->add('save', SubmitType::class);
            
            
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       
        $resolver
        ->setDefaults([
            'data_class' => Zutat::class,
        ])
        ->setDefault('lieferanten', null)
        ->setRequired('lieferanten')
        ->setAllowedTypes('lieferanten', array('array'))
        ->setDefault('warengruppe', null)
        ->setRequired('warengruppe')
        ->setAllowedTypes('warengruppe', array('array'))
        ->setDefault('einheit', null)
        ->setRequired('einheit')
        ->setAllowedTypes('einheit', array('array'))
        ;
    }
}
