<?php

namespace App\Form;

use App\Entity\Lieferant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LieferantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Firmenname')
            ->add('Alias')
            ->add('Adresse')
            ->add('PLZ')
            ->add('Ort')
            ->add('Phone')
            ->add('Ansprechpartner', null, ['required' => false,])
            ->add('Mail')
            ->add('HerstellerOrLieferant', ChoiceType::class, [
                'choices' => [
                    'Hersteller' => Lieferant::IS_HERSTELLER,
                    'Lieferant' => Lieferant::IS_LIEFERANT,
                ],
                'label' => 'Ist es Hersteller oder Lieferant?',
                'attr' => array('class' => 'select2'),
                'multiple' => false
            ])
            //->add('Date_added')
            //->add('User_added')
            //->add('zutats')
            ->add('save', SubmitType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieferant::class,
        ])/* ->setDefault('lieferanten', null)
        ->setAllowedTypes('lieferanten', array('array'))
        ->setDefault('warengruppe', null)
        ->setAllowedTypes('warengruppe', array('array'))
        ->setDefault('zutaten', null)
        ->setAllowedTypes('zutaten', array('array'))*/
        ;
    }
}
