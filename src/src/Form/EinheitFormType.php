<?php

namespace App\Form;

use App\Entity\Einheit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EinheitFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name',null, ['attr' => ['class' => 'tinymce']])
            ->add('save', SubmitType::class, ['attr' => ['class' => 'tinymce']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Einheit::class,
        ]);
    }
}
