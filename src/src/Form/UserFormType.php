<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType; Disabled Submit Button - Using Submit Button in Template
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            //->add('roles')
            //->add('password')
            ->add('DisplayName', TextType::class, ['label' => 'Benutzername'])
            ->add('username',TextType::class, ['label' => 'Passwort'])
            ->add('Signature', HiddenType::class, [
                'data' => '',
                //'attr' => ['class' => 'pick_zutat'],
            ])
            ->add('isAdmin', CheckboxType::class, [
                'label'    => 'Benutze ist Administrator',
                'required' => false,
            ])
            ->add('isInventoryAuthorized', CheckboxType::class, [
                'label'    => 'Benutze ist berechtigt Inventurdaten zu ändern',
                'required' => false,
            ])
            //->add('save', SubmitType::class); Disabled Submit Button - Using Submit Button in Template
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
