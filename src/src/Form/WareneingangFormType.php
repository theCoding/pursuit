<?php

namespace App\Form;

use App\Entity\Wareneingang;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class WareneingangFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('Zutat', ChoiceType::class, [
                'choices'  => [
                    'Maybe' => null,
                    'Yes' => true,
                    'No' => false,
                ],
            ])*/
            ->add('Charge')
            ->add('Qualitaetscheck', null, [
                'data' => true,
                'label' => 'Optische Sauberkeit der Anlieferunggseinnheit, frei vonn Schädlingen, Schimmel'
            ])
            ->add('Identitatsprufung', null, [
                'data' => true,
                'label' => 'Identitatsprüfung Annggaben auf dem Lieferschein entsprechen den Angabenn auf dem Rohstoff/Material'
            ])
            ->add('Menge')
            ->add('Lieferschein')
            ->add('ZutatInt', ChoiceType::class, [
                'choices'  => $options['zutaten'],
                'label' => 'Zutat',
            ])
            ->add('LieferInt', ChoiceType::class, [
                'choices'  => $options['lieferanten'],
                'label' => 'Lieferant',
            ])
            
            //->add('Date_added')
            //->add('User_added')
            /*->add('Inventur', CheckboxType::class, [
                'label'    => 'Inventurdaten. Achtung! Alle bestehenden Bestände werden mit der angegebenen Menge überschrieben!',
                'required' => false,
            ])*/
            
            ->add('save', SubmitType::class);
        
    }

 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Wareneingang::class,
        ])
        ->setDefault('lieferanten', null)
        ->setRequired('lieferanten')
        ->setAllowedTypes('lieferanten', array('array'))
        ->setDefault('zutaten', null)
        ->setRequired('zutaten')
        ->setAllowedTypes('zutaten', array('array'))
        ;
    }
}
