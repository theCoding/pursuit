<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429075922 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ansatz (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, rezept_id INTEGER NOT NULL, datum DATETIME NOT NULL, status INTEGER NOT NULL, menge INTEGER NOT NULL, object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_9F743EC5A76ED395 ON ansatz (user_id)');
        $this->addSql('CREATE INDEX IDX_9F743EC534F98B88 ON ansatz (rezept_id)');
        $this->addSql('CREATE TABLE einheit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_added_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, object_state INTEGER NOT NULL, date_added DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_1EC2986E9E43ABD ON einheit (user_added_id)');
        $this->addSql('CREATE TABLE rezeptur (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_added_id INTEGER NOT NULL, bezeichnung VARCHAR(255) NOT NULL, beschreibung VARCHAR(255) NOT NULL, notiz VARCHAR(255) NOT NULL, alkoholgehalt DOUBLE PRECISION NOT NULL, date_added DATETIME NOT NULL, zutaten CLOB NOT NULL --(DC2Type:array)
        , menge CLOB NOT NULL --(DC2Type:array)
        , lieferant CLOB NOT NULL --(DC2Type:array)
        , object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_7D3601EF9E43ABD ON rezeptur (user_added_id)');
        $this->addSql('CREATE TABLE wareneingang (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, lieferant_id INTEGER DEFAULT NULL, user_added_id INTEGER NOT NULL, zutat_id INTEGER NOT NULL, charge INTEGER NOT NULL, qualitaetscheck BOOLEAN NOT NULL, menge INTEGER NOT NULL, date_added DATETIME NOT NULL, lieferschein VARCHAR(255) NOT NULL, object_state VARCHAR(255) NOT NULL, identitatsprufung BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_8C4FECA81EA829A6 ON wareneingang (lieferant_id)');
        $this->addSql('CREATE INDEX IDX_8C4FECA89E43ABD ON wareneingang (user_added_id)');
        $this->addSql('CREATE INDEX IDX_8C4FECA8AF5E977E ON wareneingang (zutat_id)');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, display_name VARCHAR(255) NOT NULL, signature VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('CREATE TABLE lieferant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_added_id INTEGER NOT NULL, firmenname VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, date_added DATETIME NOT NULL, plz VARCHAR(255) NOT NULL, ort VARCHAR(255) NOT NULL, ansprechpartner VARCHAR(255) DEFAULT NULL, object_state INTEGER NOT NULL, mail VARCHAR(255) NOT NULL, hersteller_or_lieferant SMALLINT NOT NULL)');
        $this->addSql('CREATE INDEX IDX_E13E90BA9E43ABD ON lieferant (user_added_id)');
        $this->addSql('CREATE TABLE lieferant_lieferant (lieferant_source INTEGER NOT NULL, lieferant_target INTEGER NOT NULL, PRIMARY KEY(lieferant_source, lieferant_target))');
        $this->addSql('CREATE INDEX IDX_CA7384854F08BEE7 ON lieferant_lieferant (lieferant_source)');
        $this->addSql('CREATE INDEX IDX_CA73848556EDEE68 ON lieferant_lieferant (lieferant_target)');
        $this->addSql('CREATE TABLE abfullung (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ansatz_id INTEGER NOT NULL, produkt_id INTEGER NOT NULL, user_added_id INTEGER NOT NULL, verbrauch_ansatz INTEGER NOT NULL, menge_flaschen INTEGER NOT NULL, flaschen INTEGER NOT NULL, los VARCHAR(255) NOT NULL, date DATETIME NOT NULL, object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_DBF609BCE7883F5C ON abfullung (ansatz_id)');
        $this->addSql('CREATE INDEX IDX_DBF609BC75F42D9B ON abfullung (produkt_id)');
        $this->addSql('CREATE INDEX IDX_DBF609BC9E43ABD ON abfullung (user_added_id)');
        $this->addSql('CREATE TABLE produkt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, rezept_id INTEGER NOT NULL, user_added_id INTEGER NOT NULL, bezeichnung VARCHAR(255) NOT NULL, date_added DATETIME NOT NULL, kunde VARCHAR(255) DEFAULT NULL, object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_1B938EA534F98B88 ON produkt (rezept_id)');
        $this->addSql('CREATE INDEX IDX_1B938EA59E43ABD ON produkt (user_added_id)');
        $this->addSql('CREATE TABLE warengruppe (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_added_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, date_added DATETIME NOT NULL, object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_9378B3749E43ABD ON warengruppe (user_added_id)');
        $this->addSql('CREATE TABLE warengruppe_warengruppe (warengruppe_source INTEGER NOT NULL, warengruppe_target INTEGER NOT NULL, PRIMARY KEY(warengruppe_source, warengruppe_target))');
        $this->addSql('CREATE INDEX IDX_3A6618144497E857 ON warengruppe_warengruppe (warengruppe_source)');
        $this->addSql('CREATE INDEX IDX_3A6618145D72B8D8 ON warengruppe_warengruppe (warengruppe_target)');
        $this->addSql('CREATE TABLE zutat (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_added_id INTEGER DEFAULT NULL, einheit VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, menge INTEGER NOT NULL, notiz VARCHAR(255) DEFAULT NULL, date_added DATETIME NOT NULL, object_state VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_51D770929E43ABD ON zutat (user_added_id)');
        $this->addSql('CREATE TABLE zutat_lieferant (zutat_id INTEGER NOT NULL, lieferant_id INTEGER NOT NULL, PRIMARY KEY(zutat_id, lieferant_id))');
        $this->addSql('CREATE INDEX IDX_102537A0AF5E977E ON zutat_lieferant (zutat_id)');
        $this->addSql('CREATE INDEX IDX_102537A01EA829A6 ON zutat_lieferant (lieferant_id)');
        $this->addSql('CREATE TABLE zutat_warengruppe (zutat_id INTEGER NOT NULL, warengruppe_id INTEGER NOT NULL, PRIMARY KEY(zutat_id, warengruppe_id))');
        $this->addSql('CREATE INDEX IDX_6BE78EFFAF5E977E ON zutat_warengruppe (zutat_id)');
        $this->addSql('CREATE INDEX IDX_6BE78EFFA48F015A ON zutat_warengruppe (warengruppe_id)');
        $this->addSql('CREATE TABLE updates (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, number DOUBLE PRECISION DEFAULT NULL, available BOOLEAN NOT NULL, content VARCHAR(1000) DEFAULT NULL, date DATETIME NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ansatz');
        $this->addSql('DROP TABLE einheit');
        $this->addSql('DROP TABLE rezeptur');
        $this->addSql('DROP TABLE wareneingang');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE lieferant');
        $this->addSql('DROP TABLE lieferant_lieferant');
        $this->addSql('DROP TABLE abfullung');
        $this->addSql('DROP TABLE produkt');
        $this->addSql('DROP TABLE warengruppe');
        $this->addSql('DROP TABLE warengruppe_warengruppe');
        $this->addSql('DROP TABLE zutat');
        $this->addSql('DROP TABLE zutat_lieferant');
        $this->addSql('DROP TABLE zutat_warengruppe');
        $this->addSql('DROP TABLE updates');
    }
}
