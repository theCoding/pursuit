<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use App\Entity\Traits\DateAndUserAddedTrait;
use App\Entity\Traits\FieldAliasTrait;
use App\Entity\Traits\FieldNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WarengruppeRepository")
 */
class Warengruppe extends AbstractEntity {


    use FieldNameTrait;
    use DateAndUserAddedTrait;
    use FieldAliasTrait;

    const
        TITLE_FOR_COLUMN_NAME = "Name",
        TITLE_FOR_COLUMN_UEBERGRUPPE = "Übergruppen",
        TITLE_FOR_COLUMN_WARENGRUPPES = "Untergruppen",
        TITLE_FOR_COLUMN_ZUTATEN = "Zutaten",
        TITLE_FOR_COLUMN_DATE_ADDED = "Hinzugefüg am",
        TITLE_FOR_COLUMN_USER_ADDED = "Angelegt von";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Warengruppe", inversedBy="warengruppes")
     */
    protected $Uebergruppe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Warengruppe", mappedBy="Uebergruppe")
     */
    protected $warengruppes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Zutat", mappedBy="Gruppe")
     */
    protected $zutaten;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $DateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="createdwarengruppe")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $UserAdded;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;

    public function __construct() {
        $this->Uebergruppe  = new ArrayCollection();
        $this->warengruppes = new ArrayCollection();
        $this->zutaten      = new ArrayCollection();
    }


    /**
     * @return Collection|self[]
     */
    public function getUebergruppe(): Collection {
        return $this->Uebergruppe;
    }

    public function addUebergruppe(self $uebergruppe): self {
        if (! $this->Uebergruppe->contains($uebergruppe)) {
            $this->Uebergruppe[] = $uebergruppe;
        }

        return $this;
    }

    public function removeUebergruppe(self $uebergruppe): self {
        if ($this->Uebergruppe->contains($uebergruppe)) {
            $this->Uebergruppe->removeElement($uebergruppe);
        }

        return $this;
    }

    protected $GruppeInt = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $ObjectState;

    public function setGruppeInt($gruppeInt) {
        $this->GruppeInt = $gruppeInt;

        return $this;
    }

    public function getGruppeInt() {
        return $this->GruppeInt;
    }

    public function makeGruppeInt($gruppeArray) {
        /*if($this->GruppeInt != -1){
            $this->addUebergruppe($gruppeArray[$this->GruppeInt]);
        }else{
            $this->Uebergruppe = null;
        }*/
        $this->Uebergruppe = new ArrayCollection();
        if (sizeof($this->GruppeInt) > 0) {
            foreach ($this->GruppeInt as $intg) {
                if ($intg >= 0) {
                    $this->addUebergruppe($gruppeArray[$intg]);
                }
            }
        } else {
            $this->Uebergruppe = new ArrayCollection();
        }
    }

    public function reconstructGruppeInt($gruppeArray) {
        $gruppe = $this->getUebergruppe();

        if ($gruppe != null) {
            foreach ($gruppe as $grup) {
                array_push($this->GruppeInt, $gruppeArray[$grup->getName()]);
                //$this->GruppeInt = $gruppeArray[$gruppe[0]->getName()];
            }
        } else {
            $this->GruppeInt = null;
        }
    }

    /**
     * @return Collection|self[]
     */
    public function getWarengruppes(): Collection {
        return $this->warengruppes;
    }

    public function addWarengruppe(self $warengruppe): self {
        if (! $this->warengruppes->contains($warengruppe)) {
            $this->warengruppes[] = $warengruppe;
            $warengruppe->addUebergruppe($this);
        }

        return $this;
    }

    public function removeWarengruppe(self $warengruppe): self {
        if ($this->warengruppes->contains($warengruppe)) {
            $this->warengruppes->removeElement($warengruppe);
            $warengruppe->removeUebergruppe($this);
        }

        return $this;
    }

    /**
     * @return Collection|Zutat[]
     */
    public function getZutaten(): Collection {
        return $this->zutaten;
    }

    public function addZutaten(Zutat $zutaten): self {
        if (! $this->zutaten->contains($zutaten)) {
            $this->zutaten[] = $zutaten;
            $zutaten->addGruppe($this);
        }

        return $this;
    }

    public function removeZutaten(Zutat $zutaten): self {
        if ($this->zutaten->contains($zutaten)) {
            $this->zutaten->removeElement($zutaten);
            $zutaten->removeGruppe($this);
        }

        return $this;
    }



    //   "Name", "Übergruppen", "Untergruppen", "Zutaten", "Hinzugefüg am", "Angelegt von"

    public function getFieldListWithTitles() {
        $result                              = [];
        $result[self::TITLE_FOR_COLUMN_NAME] = $this->getName();
        $result[self::TITLE_FOR_COLUMN_UEBERGRUPPE] = self::convertCollectionToNameString($this->getUebergruppe());
        $result[self::TITLE_FOR_COLUMN_WARENGRUPPES] = self::convertCollectionToNameString($this->getWarengruppes());
        $result[self::TITLE_FOR_COLUMN_ZUTATEN] = self::convertCollectionToNameString($this->getZutaten());
        $result[self::TITLE_FOR_COLUMN_DATE_ADDED] = $this->getDateAdded()->format('Y-m-d H:i:s');
        $result[self::TITLE_FOR_COLUMN_USER_ADDED] = $this->getUserAdded()->getDisplayName();


        return $result;
    }

}
