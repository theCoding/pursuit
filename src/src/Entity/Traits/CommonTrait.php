<?php

namespace App\Entity\Traits;

trait CommonTrait {
    protected $ObjectState;
    protected $id;
    protected $DateEdit;
    protected $EditorId;

    public function getObjectState(): ?int {
        return $this->ObjectState;
    }

    public function setObjectState(int $ObjectState): self {
        $this->ObjectState = $ObjectState;

        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }



    public function setEditorId(int $id){
        $this->EditorId = $id;
        return $this;
    }
    public function getEditorId(){
        return $this->EditorId ;
    }


    public function getDateEdit(): ?\DateTimeInterface {
        return $this->DateEdit;
    }

    public function setDateEdit(\DateTimeInterface $DateEdit): self {
        $this->DateEdit = $DateEdit;
        return $this;
    }
}