<?php
namespace App\Entity\Traits;
use App\Entity\User;

trait DateAndUserAddedTrait{


    public function getDateAdded(): ?\DateTimeInterface {
        return $this->DateAdded;
    }

    public function setDateAdded(\DateTimeInterface $DateAdded): self {
        $this->DateAdded = $DateAdded;

        return $this;
    }

    public function getUserAdded(): ?User {
        return $this->UserAdded;
    }

    public function setUserAdded(?User $UserAdded): self {
        $this->UserAdded = $UserAdded;

        return $this;
    }
}