<?php
namespace App\Entity\Traits;

trait FieldAliasTrait {

    public function getAlias(): ?string {
        return $this->Alias;
    }

    public function setAlias(string $Alias): self {
        $this->Alias = $Alias;

        return $this;
    }
}