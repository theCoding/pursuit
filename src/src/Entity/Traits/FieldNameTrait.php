<?php
namespace App\Entity\Traits;

trait FieldNameTrait {

    protected $Name;

    public function getName(): ?string {
        return $this->Name;
    }

    public function setName(string $Name): self {
        $this->Name = $Name;

        return $this;
    }

}