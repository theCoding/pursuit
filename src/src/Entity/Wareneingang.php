<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use App\Entity\Traits\DateAndUserAddedTrait;
use App\Entity\Traits\FieldAliasTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WareneingangRepository")
 */
class Wareneingang extends AbstractEntity {


    use DateAndUserAddedTrait;
    use FieldAliasTrait;

    const
        TITLE_FOR_COLUMN_ZUTAT = "Zutat",
        TITLE_FOR_COLUMN_CHARGE = "Charge",
        TITLE_FOR_COLUMN_LIEFERSCHEIN = "Lieferschein",
        TITLE_FOR_COLUMN_QUALITATCHECK = "Qualitätscheck",
        TITLE_FOR_COLUMN_MENGE = "Menge",
        TITLE_FOR_COLUMN_LIEFERANT = "Lieferant",
        TITLE_FOR_COLUMN_DATE_ADDED = "Hinzugefüg am",
        TITLE_FOR_COLUMN_USERADDED_ADDED = "Angelegt von";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $Charge;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $Qualitaetscheck;

    /**
     * @ORM\Column(type="integer")
     */
    protected $Menge;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lieferant", inversedBy="wareneingangs")
     */
    protected $Lieferant;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $DateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="warenannahme")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $UserAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zutat", inversedBy="wareneingangs")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $Zutat;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;


    public function getId(): ?int {
        return $this->id;
    }

    public function getCharge(): ?int {
        return $this->Charge;
    }

    public function setCharge(int $Charge): self {
        $this->Charge = $Charge;

        return $this;
    }

    public function getQualitaetscheck(): ?bool {
        return $this->Qualitaetscheck;
    }

    public function setQualitaetscheck(bool $Qualitaetscheck): self {
        $this->Qualitaetscheck = $Qualitaetscheck;

        return $this;
    }

    public function getMenge(): ?int {
        return $this->Menge;
    }

    public function setMenge(int $Menge): self {
        $this->Menge = $Menge;

        return $this;
    }

    public function getLieferant(): ?Lieferant {
        return $this->Lieferant;
    }

    public function setLieferant(?Lieferant $Lieferant): self {
        $this->Lieferant = $Lieferant;

        return $this;
    }

    protected $LieferInt;

    public function setLieferInt(int $lieferInt) {
        $this->LieferInt = $lieferInt;

        return $this;
    }

    public function getLieferInt() {
        return $this->LieferInt;
    }

    public function makeLieferInt($lieferArray) {
        $this->setLieferant($lieferArray[$this->LieferInt]);
    }

    public function reconstructLieferInt($lieferArray) {
        $liefer          = $this->getLieferant();
        $this->LieferInt = $lieferArray[$liefer->getAlias()];
    }


    protected $Inventur;

    public function setInventur(int $inventory) {
        $this->Inventur = $inventory;

        return $this;
    }

    public function getInventur() {
        return $this->Inventur;
    }


    public function getZutat(): ?Zutat {
        return $this->Zutat;
    }

    public function setZutat(?Zutat $Zutat): self {

        $this->Zutat = $Zutat;

        return $this;
    }

    protected $ZutatInt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Lieferschein;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $ObjectState;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $Identitatsprufung;

    public function setZutatInt($zutatInt) {
        $this->ZutatInt = $zutatInt;

        return $this;
    }

    public function getZutatInt() {
        return $this->ZutatInt;
    }

    public function makeZutatInt($zutatArray) {

        // dump($zutatArray);
        //dump($this->ZutatInt);
        //
        if (is_int($this->ZutatInt)) {
            $this->setZutat($zutatArray[$this->ZutatInt]);
        } else {
            $this->setZutat($this->ZutatInt);
        }
    }

    public function reconstructZutatInt($zutatArray) {
        $zutat = $this->getZutat();

        $this->ZutatInt = $zutatArray[$zutat->getName()];
    }


    public function getLieferschein(): ?string {
        return $this->Lieferschein;
    }

    public function setLieferschein(string $Lieferschein): self {
        $this->Lieferschein = $Lieferschein;

        return $this;
    }


    public function getIdentitatsprufung(): ?bool {
        return $this->Identitatsprufung;
    }

    public function setIdentitatsprufung(bool $Identitatsprufung): self {
        $this->Identitatsprufung = $Identitatsprufung;

        return $this;
    }



    public function getFieldListWithTitles() {
        $result                                         = [];
        $result[self::TITLE_FOR_COLUMN_ZUTAT]           = $this->getZutat()->getName();
        $result[self::TITLE_FOR_COLUMN_CHARGE]          = $this->getCharge();
        $result[self::TITLE_FOR_COLUMN_LIEFERSCHEIN]    = $this->getLieferschein();
        $result[self::TITLE_FOR_COLUMN_QUALITATCHECK]   = $this->getQualitaetscheck();
        $result[self::TITLE_FOR_COLUMN_MENGE]           = $this->getMenge();
        $result[self::TITLE_FOR_COLUMN_LIEFERANT]       = $this->getLieferant()->getAlias();
        $result[self::TITLE_FOR_COLUMN_DATE_ADDED]      = $this->getDateAdded()->format("Y-m-d H:i:s");
        $result[self::TITLE_FOR_COLUMN_USERADDED_ADDED] = $this->getUserAdded()->getDisplayName();


        return $result;
    }
}
