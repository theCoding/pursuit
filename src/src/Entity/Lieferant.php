<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use App\Entity\Traits\DateAndUserAddedTrait;
use App\Entity\Traits\FieldAliasTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LieferantRepository")
 *
 */
class Lieferant extends AbstractEntity {


    use DateAndUserAddedTrait;
    use FieldAliasTrait;

    const IS_HERSTELLER = 1;
    const IS_LIEFERANT  = 2;

    const IS_HERSTELLER_OR_LIEFERANT_TITLES = [
        self::IS_HERSTELLER => 'Hersteller',
        self::IS_LIEFERANT  => 'Lieferant',
    ];


    const
        TITLE_FOR_COLUMN_FIRMNAME = "Name",
        TITLE_FOR_COLUMN_ADRESSE = "Adresse",
        TITLE_FOR_COLUMN_PHONE = "Telefon",
        TITLE_FOR_COLUMN_ANSPRECHPARTNER = "Ansprechpartner",
        TITLE_FOR_COLUMN_MAIL = "e-Mai",
        TITLE_FOR_COLUMN_DATE_ADDED = "Hinzugefüg am",
        TITLE_FOR_COLUMN_USER_ADDED = "Angelegt von",
        TITLE_FOR_COLUMN_ZUTATEN = "Zutaten",
        TITLE_FOR_COLUMN_WARENEINGANG = "Wareneingänge",
        TITLE_FOR_COLUMN_HERSTELLER = "Hersteller",
        TITLE_FOR_COLUMN_HERSTELLER_OR_LIFERANT = "Hersteller",
        TITLE_FOR_COLUMN_ALIAS = "Alias";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Firmenname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Alias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Phone;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $DateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="lieferants")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $UserAdded;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Zutat", mappedBy="Lieferant")
     */
    protected $zutats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wareneingang", mappedBy="Lieferant")
     */
    protected $wareneingangs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $PLZ;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Ort;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $Ansprechpartner;

    /**
     * @ORM\Column(type="integer")
     */
    protected $ObjectState;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Mail;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lieferant")
     */
    protected $Hersteller;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $HerstellerOrLieferant;
    // 0 = entwurf, 1 = in bearbeitung, 2 = veröffentlicht, 3 = archiviert, 4 = gelöscht

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;




    public function __construct() {
        $this->zutats        = new ArrayCollection();
        $this->wareneingangs = new ArrayCollection();
        $this->Hersteller    = new ArrayCollection();
    }


    public function getFirmenname(): ?string {
        return $this->Firmenname;
    }

    public function setFirmenname(string $Firmenname): self {
        $this->Firmenname = $Firmenname;

        return $this;
    }



    public function getAdresse(): ?string {
        return $this->Adresse;
    }

    public function setAdresse(string $Adresse): self {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function getPhone(): ?string {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self {
        $this->Phone = $Phone;

        return $this;
    }


    /**
     * @return Collection|Zutat[]
     */
    public function getZutats(): Collection {
        return $this->zutats;
    }

    public function addZutat(Zutat $zutat): self {
        if (! $this->zutats->contains($zutat)) {
            $this->zutats[] = $zutat;
            $zutat->addLieferant($this);
        }

        return $this;
    }

    public function removeZutat(Zutat $zutat): self {
        if ($this->zutats->contains($zutat)) {
            $this->zutats->removeElement($zutat);
            $zutat->removeLieferant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Wareneingang[]
     */
    public function getWareneingangs(): Collection {
        return $this->wareneingangs;
    }

    public function addWareneingang(Wareneingang $wareneingang): self {
        if (! $this->wareneingangs->contains($wareneingang)) {
            $this->wareneingangs[] = $wareneingang;
            $wareneingang->setLieferant($this);
        }

        return $this;
    }

    public function removeWareneingang(Wareneingang $wareneingang): self {
        if ($this->wareneingangs->contains($wareneingang)) {
            $this->wareneingangs->removeElement($wareneingang);
            // set the owning side to null (unless already changed)
            if ($wareneingang->getLieferant() === $this) {
                $wareneingang->setLieferant(null);
            }
        }

        return $this;
    }

    public function getPLZ(): ?string {
        return $this->PLZ;
    }

    public function setPLZ(string $PLZ): self {
        $this->PLZ = $PLZ;

        return $this;
    }

    public function getOrt(): ?string {
        return $this->Ort;
    }

    public function setOrt(string $Ort): self {
        $this->Ort = $Ort;

        return $this;
    }

    public function getAnsprechpartner(): ?string {
        return $this->Ansprechpartner;
    }

    public function setAnsprechpartner(?string $Ansprechpartner): self {
        $this->Ansprechpartner = $Ansprechpartner;

        return $this;
    }


    public function getMail(): ?string {
        return $this->Mail;
    }

    public function setMail(string $Mail): self {
        $this->Mail = $Mail;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getHersteller(): Collection {
        return $this->Hersteller;
    }

    public function addHersteller(self $hersteller): self {
        if (! $this->Hersteller->contains($hersteller)) {
            $this->Hersteller[] = $hersteller;
        }

        return $this;
    }

    public function removeHersteller(self $hersteller): self {
        if ($this->Hersteller->contains($hersteller)) {
            $this->Hersteller->removeElement($hersteller);
        }

        return $this;
    }

    public function getHerstellerOrLieferant(): ?int {
        return $this->HerstellerOrLieferant;
    }

    public function setHerstellerOrLieferant(int $HerstellerOrLieferant): self {
        $this->HerstellerOrLieferant = $HerstellerOrLieferant;

        return $this;
    }



    public function getFieldListWithTitles() {
        $result                                  = [];
        $result[self::TITLE_FOR_COLUMN_FIRMNAME] = $this->getFirmenname();
        $result[self::TITLE_FOR_COLUMN_ALIAS]  = $this->getAlias();
        $result[self::TITLE_FOR_COLUMN_ADRESSE]  = $this->getAdresse();
        $result[self::TITLE_FOR_COLUMN_PHONE]  = $this->getPhone();
        $result[self::TITLE_FOR_COLUMN_ANSPRECHPARTNER]  = $this->getAnsprechpartner();
        $result[self::TITLE_FOR_COLUMN_MAIL]  = $this->getMail();
        $result[self::TITLE_FOR_COLUMN_DATE_ADDED]  = $this->getDateAdded()->format("Y-m-d H:i:s");
        $result[self::TITLE_FOR_COLUMN_USER_ADDED]  = $this->getUserAdded()->getDisplayName();
        $result[self::TITLE_FOR_COLUMN_ZUTATEN]  = self::convertCollectionToNameString($this->getZutats());
        $result[self::TITLE_FOR_COLUMN_WARENEINGANG]  = self::convertCollectionToIdString($this->getWareneingangs());
//        $result[self::TITLE_FOR_COLUMN_HERSTELLER]  = self::convertCollectionToAliasesString($this->getHersteller());
        $result[self::TITLE_FOR_COLUMN_HERSTELLER_OR_LIFERANT]  = self::IS_HERSTELLER_OR_LIEFERANT_TITLES[$this->getHerstellerOrLieferant()];


        return $result;
    }
}
