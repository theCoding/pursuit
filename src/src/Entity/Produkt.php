<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduktRepository")
 */
class Produkt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Bezeichnung;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rezeptur", inversedBy="produkts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Rezept;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date_added;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="produkts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User_added;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Kunde;



    
    

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Abfullung", mappedBy="Produkt")
     */
    private $abfullungs;

    public function __construct()
    {
       
        $this->abfullungs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBezeichnung(): ?string
    {
        return $this->Bezeichnung;
    }


    public function setBezeichnung(string $Bezeichnung): self
    {
        $this->Bezeichnung = $Bezeichnung;

        return $this;
    }

    public function getRezept(): ?Rezeptur
    {
        return $this->Rezept;
    }

    public function setRezept(?Rezeptur $Rezept): self
    {
        $this->Rezept = $Rezept;

        return $this;
    }
    private $RezeptInt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ObjectState;
    public function setRezeptInt(int $rezeptInt){
        $this->RezeptInt = $rezeptInt;
        return $this;
    }
    public function getRezeptInt(){
        return $this->RezeptInt;
    }
    public function makeRezeptInt($rezeptArray){
        $this->setRezept($rezeptArray[$this->RezeptInt]);
    }
    public function reconstructRezeptInt($rezeptArray){
        $rezept = $this->getRezept();
        
        $this->RezeptInt = $rezeptArray[$rezept->getBezeichnung()];
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->Date_added;
    }

    public function setDateAdded(\DateTimeInterface $Date_added): self
    {
        $this->Date_added = $Date_added;

        return $this;
    }

    public function getUserAdded(): ?User
    {
        return $this->User_added;
    }

    public function setUserAdded(?User $User_added): self
    {
        $this->User_added = $User_added;

        return $this;
    }

    public function getKunde(): ?string
    {
        return $this->Kunde;
    }

    public function setKunde(?string $Kunde): self
    {
        $this->Kunde = $Kunde;

        return $this;
    }



    /**
     * @return Collection|Abfullung[]
     */
    public function getAbfullungs(): Collection
    {
        return $this->abfullungs;
    }

    public function addAbfullung(Abfullung $abfullung): self
    {
        if (!$this->abfullungs->contains($abfullung)) {
            $this->abfullungs[] = $abfullung;
            $abfullung->setProdukt($this);
        }

        return $this;
    }

    public function removeAbfullung(Abfullung $abfullung): self
    {
        if ($this->abfullungs->contains($abfullung)) {
            $this->abfullungs->removeElement($abfullung);
            // set the owning side to null (unless already changed)
            if ($abfullung->getProdukt() === $this) {
                $abfullung->setProdukt(null);
            }
        }

        return $this;
    }

    public function getObjectState(): ?string
    {
        return $this->ObjectState;
    }

    public function setObjectState(string $ObjectState): self
    {
        $this->ObjectState = $ObjectState;

        return $this;
    }
}
