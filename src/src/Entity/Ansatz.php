<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnsatzRepository")
 */
class Ansatz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Datum;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ansatzs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="integer")
     */
    private $Status;

    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Abfullung", mappedBy="Ansatz")
     */
    private $abfullungs;

    public function __construct()
    {
        $this->abfullungs = new ArrayCollection();
    }

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatum(): ?\DateTimeInterface
    {
        return $this->Datum;
    }

    public function setDatum(\DateTimeInterface $Datum): self
    {
        $this->Datum = $Datum;

        return $this;
    }
    public function setDateAdded(\DateTimeInterface $Datum): self
    {
        $this->Datum = $Datum;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
    public function setUserAdded(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->Status;
    }

    public function getStatusString(): ?string{
        switch ($this->Status) {
            case 0:
                return "Leer";
                break;
            case 1:
                return "Fertiggestellt und Abfüllbereit";
                break;
            case 2:
                return "In zubereitung";
                break;
            case 3:
                return "geplant";
                break;
        }
    }

    public function setStatus(int $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    /*public function getProdukt(): ?Produkt
    {
        return $this->Produkt;
    }

    public function setProdukt(?Produkt $Produkt): self
    {
        $this->Produkt = $Produkt;

        return $this;
    }
*/
    private $ProduktInt;

    /**
     * @ORM\Column(type="integer")
     */
    private $Menge;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rezeptur", inversedBy="ansatzs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Rezept;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ObjectState;

    //REZEPT!!!!!!! HIER IST IMMER REZEPT GEMEINNT
    public function setProduktInt(int $produktInt){
        $this->ProduktInt = $produktInt;
        return $this;
    }
    public function getProduktInt(){
        return "";
    }
    public function makeProduktInt($produktArray){
        $this->setRezept($produktArray[$this->ProduktInt]);
    }
    public function reconstructRezeptInt($rezeptArray){
        $rezept = $this->getRezept();
        
        $this->ProduktInt = $rezeptArray[$rezept->getBezeichnung()];
    }
    
    /**
     * @return Collection|Abfullung[]
     */
    public function getAbfullungs(): Collection
    {
        return $this->abfullungs;
    }

    public function addAbfullung(Abfullung $abfullung): self
    {
        if (!$this->abfullungs->contains($abfullung)) {
            $this->abfullungs[] = $abfullung;
            $abfullung->setAnsatz($this);
        }

        return $this;
    }

    public function removeAbfullung(Abfullung $abfullung): self
    {
        if ($this->abfullungs->contains($abfullung)) {
            $this->abfullungs->removeElement($abfullung);
            // set the owning side to null (unless already changed)
            if ($abfullung->getAnsatz() === $this) {
                $abfullung->setAnsatz(null);
            }
        }

        return $this;
    }

    public function getMenge(): ?int
    {
        return $this->Menge;
    }

    public function setMenge(int $Menge): self
    {
        $this->Menge = $Menge;

        return $this;
    }

    public function getRezept(): ?Rezeptur
    {
        return $this->Rezept;
    }

    public function setRezept(?Rezeptur $Rezept): self
    {
        $this->Rezept = $Rezept;

        return $this;
    }

    public function getObjectState(): ?string
    {
        return $this->ObjectState;
    }

    public function setObjectState(string $ObjectState): self
    {
        $this->ObjectState = $ObjectState;

        return $this;
    }

   
}
