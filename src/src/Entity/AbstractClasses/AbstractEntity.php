<?php

namespace App\Entity\AbstractClasses;


use App\Entity\Traits\CommonTrait;
use App\Entity\Warengruppe;
use Doctrine\Common\Collections\Collection;

abstract class AbstractEntity {

    use CommonTrait;
    const
        STATE_BLOCKED = 1,
        STATE_ENABLED = 2,
        STATE_ARCHIVED = 3,
        STATE_ERROR = 4,
        STATE_ADM_LOCKED = 5,
        STATE_ADM_DELETED = 5;

    /**
     * @param self $entity
     */
    public static function Remove(object $entity){
        $entity->setObjectState(self::STATE_ADM_DELETED);
    }

    /**
     * @param self $entity
     */
    public static function Archive(object $entity) {
        $entity->setObjectState(self::STATE_ARCHIVED);
    }


    static function convertCollectionToAliasesString(Collection $collection){
        return  implode(', ', array_map(function ($entity) {
            return $entity->getAlias();
        }, $collection->getValues()));
    }

    static function convertCollectionToNameString(Collection $collection){
        return  implode(', ', array_map(function ($entity) {
            return $entity->getName();
        }, $collection->getValues()));
    }

    static function convertCollectionToIdString(Collection $collection){
        return  implode(', ', array_map(function ($entity) {
            return $entity->getId();
        }, $collection->getValues()));
    }
}