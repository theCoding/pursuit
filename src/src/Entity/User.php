<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

//use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends AbstractEntity implements UserInterface {



    const
        TITLE_FOR_COLUMN_NAME = "Name",
        TITLE_FOR_COLUMN_ROLLEN = "Rollen",
        TITLE_FOR_COLUMN_SIGNATUR = "Signatur";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lieferant", mappedBy="UserAdded")
     */
    protected $lieferants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Zutat", mappedBy="UserAdded")
     */
    protected $zutats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wareneingang", mappedBy="UserAdded")
     */
    protected $warenannahme;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Warengruppe", mappedBy="UserAdded")
     */
    protected $createdwarengruppe;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rezeptur", mappedBy="User_added")
     */
    protected $rezepturs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ansatz", mappedBy="User")
     */
    protected $ansatzs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produkt", mappedBy="User_added")
     */
    protected $produkts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Abfullung", mappedBy="User_added")
     */
    protected $abfullungs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $DisplayName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Signature;

    /**
     * @ORM\Column(type="string", length=255, options={"default":"2"})
     */
    protected $ObjectState;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;

    public function __construct() {
        $this->lieferants         = new ArrayCollection();
        $this->zutats             = new ArrayCollection();
        $this->warenannahme       = new ArrayCollection();
        $this->createdwarengruppe = new ArrayCollection();
        $this->rezepturs          = new ArrayCollection();
        $this->ansatzs            = new ArrayCollection();
        $this->produkts           = new ArrayCollection();
        $this->abfullungs         = new ArrayCollection();
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return (string) $this->username;
        //return $this->id;
    }

    public function setUsername(string $username): self {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {

        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self {
        $this->roles = $roles;

        return $this;
    }

    protected $administrator = false;

    public function setisAdmin($admin) {
        $this->administrator = $admin;
    }

    public function getisAdmin() {
        return false;
    }

    public function makeAdmin() {

        if ($this->administrator) {
            $this->roles = ["ROLE_ADMIN"];
        } else {
            $this->roles = [];
        }
    }

    protected $inventory_authorized = false;

    public function setisInventoryAuthorized($admin) {
        $this->inventory_authorized = $admin;
    }

    public function getisInventoryAuthorized() {
        return false;
    }

    public function makeInventoryAuthorized() {

        if ($this->inventory_authorized) {

            array_push($this->roles, "ROLE_INVENTORY");
        } else {
            $this->roles = [];
        }
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return (string) $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt() {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Lieferant[]
     */
    public function getLieferants(): Collection {
        return $this->lieferants;
    }

    public function addLieferant(Lieferant $lieferant): self {
        if (! $this->lieferants->contains($lieferant)) {
            $this->lieferants[] = $lieferant;
            $lieferant->setUserAdded($this);
        }

        return $this;
    }

    public function removeLieferant(Lieferant $lieferant): self {
        if ($this->lieferants->contains($lieferant)) {
            $this->lieferants->removeElement($lieferant);
            // set the owning side to null (unless already changed)
            if ($lieferant->getUserAdded() === $this) {
                $lieferant->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Zutat[]
     */
    public function getZutats(): Collection {
        return $this->zutats;
    }

    public function addZutat(Zutat $zutat): self {
        if (! $this->zutats->contains($zutat)) {
            $this->zutats[] = $zutat;
            $zutat->setUserAdded($this);
        }

        return $this;
    }

    public function removeZutat(Zutat $zutat): self {
        if ($this->zutats->contains($zutat)) {
            $this->zutats->removeElement($zutat);
            // set the owning side to null (unless already changed)
            if ($zutat->getUserAdded() === $this) {
                $zutat->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Wareneingang[]
     */
    public function getWarenannahme(): Collection {
        return $this->warenannahme;
    }

    public function addWarenannahme(Wareneingang $warenannahme): self {
        if (! $this->warenannahme->contains($warenannahme)) {
            $this->warenannahme[] = $warenannahme;
            $warenannahme->setUserAdded($this);
        }

        return $this;
    }

    public function removeWarenannahme(Wareneingang $warenannahme): self {
        if ($this->warenannahme->contains($warenannahme)) {
            $this->warenannahme->removeElement($warenannahme);
            // set the owning side to null (unless already changed)
            if ($warenannahme->getUserAdded() === $this) {
                $warenannahme->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Warengruppe[]
     */
    public function getCreatedwarengruppe(): Collection {
        return $this->createdwarengruppe;
    }

    public function addCreatedwarengruppe(Warengruppe $createdwarengruppe): self {
        if (! $this->createdwarengruppe->contains($createdwarengruppe)) {
            $this->createdwarengruppe[] = $createdwarengruppe;
            $createdwarengruppe->setUserAdded($this);
        }

        return $this;
    }

    public function removeCreatedwarengruppe(Warengruppe $createdwarengruppe): self {
        if ($this->createdwarengruppe->contains($createdwarengruppe)) {
            $this->createdwarengruppe->removeElement($createdwarengruppe);
            // set the owning side to null (unless already changed)
            if ($createdwarengruppe->getUserAdded() === $this) {
                $createdwarengruppe->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rezeptur[]
     */
    public function getRezepturs(): Collection {
        return $this->rezepturs;
    }

    public function addRezeptur(Rezeptur $rezeptur): self {
        if (! $this->rezepturs->contains($rezeptur)) {
            $this->rezepturs[] = $rezeptur;
            $rezeptur->setUserAdded($this);
        }

        return $this;
    }

    public function removeRezeptur(Rezeptur $rezeptur): self {
        if ($this->rezepturs->contains($rezeptur)) {
            $this->rezepturs->removeElement($rezeptur);
            // set the owning side to null (unless already changed)
            if ($rezeptur->getUserAdded() === $this) {
                $rezeptur->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ansatz[]
     */
    public function getAnsatzs(): Collection {
        return $this->ansatzs;
    }

    public function addAnsatz(Ansatz $ansatz): self {
        if (! $this->ansatzs->contains($ansatz)) {
            $this->ansatzs[] = $ansatz;
            $ansatz->setUser($this);
        }

        return $this;
    }

    public function removeAnsatz(Ansatz $ansatz): self {
        if ($this->ansatzs->contains($ansatz)) {
            $this->ansatzs->removeElement($ansatz);
            // set the owning side to null (unless already changed)
            if ($ansatz->getUser() === $this) {
                $ansatz->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Produkt[]
     */
    public function getProdukts(): Collection {
        return $this->produkts;
    }

    public function addProdukt(Produkt $produkt): self {
        if (! $this->produkts->contains($produkt)) {
            $this->produkts[] = $produkt;
            $produkt->setUserAdded($this);
        }

        return $this;
    }

    public function removeProdukt(Produkt $produkt): self {
        if ($this->produkts->contains($produkt)) {
            $this->produkts->removeElement($produkt);
            // set the owning side to null (unless already changed)
            if ($produkt->getUserAdded() === $this) {
                $produkt->setUserAdded(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Abfullung[]
     */
    public function getAbfullungs(): Collection {
        return $this->abfullungs;
    }

    public function addAbfullung(Abfullung $abfullung): self {
        if (! $this->abfullungs->contains($abfullung)) {
            $this->abfullungs[] = $abfullung;
            $abfullung->setUserAdded($this);
        }

        return $this;
    }

    public function removeAbfullung(Abfullung $abfullung): self {
        if ($this->abfullungs->contains($abfullung)) {
            $this->abfullungs->removeElement($abfullung);
            // set the owning side to null (unless already changed)
            if ($abfullung->getUserAdded() === $this) {
                $abfullung->setUserAdded(null);
            }
        }

        return $this;
    }

    public function getDisplayName(): ?string {
        return $this->DisplayName;
    }

    public function setDisplayName(string $DisplayName): self {
        $this->DisplayName = $DisplayName;

        return $this;
    }

    public function getSignature(): ?string {
        return $this->Signature;
    }

    public function setSignature(string $Signature): self {
        $this->Signature = $Signature;

        return $this;
    }



    public function getFieldListWithTitles() {
        $result                                   = [];
        $result[self::TITLE_FOR_COLUMN_NAME]      = $this->getDisplayName();
        $result[self::TITLE_FOR_COLUMN_ROLLEN]      = implode(", ", $this->getRoles());

        return $result;
    }
}
