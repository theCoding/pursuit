<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use App\Entity\Traits\DateAndUserAddedTrait;
use App\Entity\Traits\FieldNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZutatRepository")
 */
class Zutat extends AbstractEntity {

    use FieldNameTrait;

    use DateAndUserAddedTrait;

    const
        TITLE_FOR_COLUMN_NAME = "Name",
        TITLE_FOR_COLUMN_LIEFERANT = "Lieferant",
        TITLE_FOR_COLUMN_MENGE = "Menge",
        TITLE_FOR_COLUMN_GRUPPE = "Gruppe",
        TITLE_FOR_COLUMN_NOTIZ = "Notiz",
        TITLE_FOR_COLUMN_DATE_ADDED = "DateAdded",
        TITLE_FOR_COLUMN_USER_ADDED = "UserAdded",
        TITLE_FOR_COLUMN_WARENEINGANGS = "Wareneingangs",
        TITLE_FOR_COLUMN_EINHEIT = "Einheit";

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Einheit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lieferant", inversedBy="zutats")
     */
    protected $Lieferant;

    /**
     * @ORM\Column(type="integer")
     */
    protected $Menge;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Warengruppe", inversedBy="zutaten")
     */
    protected $Gruppe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $Notiz;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $DateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="zutats")
     */
    protected $UserAdded;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wareneingang", mappedBy="Zutat")
     */
    protected $wareneingangs;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;

    public function __construct() {
        $this->Lieferant     = new ArrayCollection();
        $this->Gruppe        = new ArrayCollection();
        $this->wareneingangs = new ArrayCollection();
    }


    public function getEinheit(): ?string {
        return $this->Einheit;
    }

    public function setEinheit(string $Einheit): self {
        $this->Einheit = $Einheit;

        return $this;
    }


    /**
     * @return Collection|Lieferant[]
     */
    public function getLieferant(): Collection {
        return $this->Lieferant;
    }

    public function addLieferant(Lieferant $lieferant): self {
        if (! $this->Lieferant->contains($lieferant)) {
            $this->Lieferant[] = $lieferant;
        }

        return $this;
    }

    public function removeLieferant(Lieferant $lieferant): self {

        if ($this->Lieferant->contains($lieferant)) {
            $this->Lieferant->removeElement($lieferant);
        }

        return $this;
    }

    protected $LieferInt;

    public function setLieferInt($lieferInt) {
        $this->LieferInt = $lieferInt;

        return $this;
    }

    public function getLieferInt() {
        return $this->LieferInt;
    }

    public function makeLieferInt($lieferArray) {
        $this->Lieferant = new ArrayCollection();
        if (sizeof($this->LieferInt) > 0) {

            foreach ($this->LieferInt as $intl) {
                $this->addLieferant($lieferArray[$intl]);
            }
        } else {
            $this->Lieferant = new ArrayCollection();
        }
        // $this->addLieferant($lieferArray[$this->LieferInt]);
    }

    public function reconstructLieferInt($lieferArray) {
        /* $liefer = $this->getLieferant();
         $this->LieferInt = $lieferArray[$liefer[0]->getAlias()];*/
        $lieferanten = $this->getLieferant();
        if ($lieferanten != null) {
            $this->LieferInt = [];
            foreach ($lieferanten as $liefer) {
                array_push($this->LieferInt, $lieferArray[$liefer->getAlias()]);
                //$this->GruppeInt = $gruppeArray[$gruppe[0]->getName()];
            }
        } else {
            $this->LieferInt = null;
        }
    }

    public function getMenge(): ?int {
        return $this->Menge;
    }

    public function getMengeLtr(): ?int {
        if ($this->getName() == "Zucker" || $this->getEinheit() == "kg")
            return ($this->Menge) * 1.2;
    }

    public function setMenge(int $Menge): self {
        $this->Menge = $Menge;

        return $this;
    }

    /**
     * @return Collection|Warengruppe[]
     */
    public function getGruppe(): Collection {
        return $this->Gruppe;
    }

    public function addGruppe(Warengruppe $gruppe): self {
        if (! $this->Gruppe->contains($gruppe)) {
            $this->Gruppe[] = $gruppe;
        }

        return $this;
    }

    public function removeGruppe(Warengruppe $gruppe): self {
        if ($this->Gruppe->contains($gruppe)) {
            $this->Gruppe->removeElement($gruppe);
        }

        return $this;
    }

    protected $GruppeInt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $ObjectState;

    public function setGruppeInt($gruppeInt) {
        $this->GruppeInt = $gruppeInt;

        return $this;
    }

    public function getGruppeInt() {
        return $this->GruppeInt;
    }

    public function reconstructGruppeInt($gruppeArray) {
        /* $gruppe = $this->getGruppe();
         if($gruppe[0] != null){
         $this->GruppeInt = $gruppeArray[$gruppe[0]->getName()];
     }else{
         $this->GruppeInt = -1;
     }*/

        $gruppe = $this->getGruppe();
        if ($gruppe != null) {
            $this->GruppeInt = [];
            foreach ($gruppe as $grup) {
                array_push($this->GruppeInt, $gruppeArray[$grup->getName()]);
                //$this->GruppeInt = $gruppeArray[$gruppe[0]->getName()];
            }
        } else {
            $this->GruppeInt = null;
        }
    }

    public function makeGruppeInt($gruppeArray) {
        /*if($this->GruppeInt == -1){
            $this->Gruppe = null;
        }else{
            $this->addGruppe($gruppeArray[$this->GruppeInt]);
        }*/
        $this->Gruppe = new ArrayCollection();
        if (sizeof($this->GruppeInt) > 0) {
            foreach ($this->GruppeInt as $intg) {
                if ($intg >= 0) {
                    $this->addGruppe($gruppeArray[$intg]);
                }
            }
        } else {
            $this->Gruppe = new ArrayCollection();
        }
    }


    public function getNotiz(): ?string {
        return $this->Notiz;
    }

    public function setNotiz(string $Notiz): self {
        $this->Notiz = $Notiz;

        return $this;
    }


    /**
     * @return Collection|Wareneingang[]
     */
    public function getWareneingangs(): Collection {
        return $this->wareneingangs;
    }

    public function addWareneingang(Wareneingang $wareneingang): self {
        if (! $this->wareneingangs->contains($wareneingang)) {
            $this->wareneingangs[] = $wareneingang;
            $wareneingang->setZutat($this);
        }

        return $this;
    }

    public function removeWareneingang(Wareneingang $wareneingang): self {
        if ($this->wareneingangs->contains($wareneingang)) {
            $this->wareneingangs->removeElement($wareneingang);
            // set the owning side to null (unless already changed)
            if ($wareneingang->getZutat() === $this) {
                $wareneingang->setZutat(null);
            }
        }

        return $this;
    }



    public function getFieldListWithTitles() {
        $result                              = [];
        $result[self::TITLE_FOR_COLUMN_NAME] = $this->Name;
        $result[self::TITLE_FOR_COLUMN_LIEFERANT] = self::convertCollectionToAliasesString($this->getLieferant());

        $result[self::TITLE_FOR_COLUMN_MENGE] = $this->getMenge();
        $result[self::TITLE_FOR_COLUMN_GRUPPE] = self::convertCollectionToNameString($this->getGruppe());

        $result[self::TITLE_FOR_COLUMN_NOTIZ] = $this->getNotiz();

        $result[self::TITLE_FOR_COLUMN_DATE_ADDED] = $this->getDateAdded()->format("Y-m-d H:i:s");
        $result[self::TITLE_FOR_COLUMN_USER_ADDED] = $this->getUserAdded()->getDisplayName();
        $result[self::TITLE_FOR_COLUMN_WARENEINGANGS] = self::convertCollectionToIdString($this->getWareneingangs());
        $result[self::TITLE_FOR_COLUMN_EINHEIT] = $this->getEinheit();

        return $result;
    }

}
