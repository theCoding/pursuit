<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AbfullungRepository")
 */
class Abfullung
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ansatz", inversedBy="abfullungs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Ansatz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produkt", inversedBy="abfullungs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Produkt;

    /**
     * @ORM\Column(type="integer")
     */
    private $Verbrauch_Ansatz;

    /**
     * @ORM\Column(type="integer")
     */
    private $Menge_flaschen;

    /**
     * @ORM\Column(type="integer")
     */
    private $Flaschen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LOS;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="abfullungs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User_added;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnsatz(): ?Ansatz
    {
        return $this->Ansatz;
    }

    public function setAnsatz(?Ansatz $Ansatz): self
    {
        $this->Ansatz = $Ansatz;

        return $this;
    }

    private $AnsatzInt;
    public function setAnsatzInt(int $ansatzInt){
        $this->AnsatzInt = $ansatzInt;
        return $this;
    }
    public function getAnsatzInt(){
        return $this->AnsatzInt;
    }
    public function makeAnsatzInt($ansatzArray){
        $this->setAnsatz($ansatzArray[$this->AnsatzInt]);
    }
    public function reconstructAnsatzInt($ansatzArray){
        $ansatz = $this->getAnsatz();
        
        $this->AnsatzInt = $ansatzArray[$ansatz->getRezept()->getBezeichnung()];
    }

    public function getProdukt(): ?Produkt
    {
        return $this->Produkt;
    }

    public function setProdukt(?Produkt $Produkt): self
    {
        $this->Produkt = $Produkt;

        return $this;
    }

    private $ProduktInt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ObjectState;
    public function setProduktInt(int $produktInt){
        $this->ProduktInt = $produktInt;
        return $this;
    }
    public function getProduktInt(){
        return $this->ProduktInt;
    }
    public function makeProduktInt($produktArray){
        $this->setProdukt($produktArray[$this->ProduktInt]);
    }
    public function reconstructProduktInt($produktArray){
        $produkt = $this->getProdukt();
        
        $this->ProduktInt = $produktArray[$produkt->getBezeichnung()];
    }

    public function getVerbrauchAnsatz(): ?int
    {
        return $this->Verbrauch_Ansatz;
    }

    public function setVerbrauchAnsatz(int $Verbrauch_Ansatz): self
    {
        $this->Verbrauch_Ansatz = $Verbrauch_Ansatz;

        return $this;
    }

    public function getMengeFlaschen(): ?int
    {
        return $this->Menge_flaschen;
    }

    public function setMengeFlaschen(int $Menge_flaschen): self
    {
        $this->Menge_flaschen = $Menge_flaschen;

        return $this;
    }

    public function getFlaschen(): ?int
    {
        return $this->Flaschen;
    }

    public function setFlaschen(int $Flaschen): self
    {
        $this->Flaschen = $Flaschen;

        return $this;
    }

    public function getLOS(): ?string
    {
        return $this->LOS;
    }

    public function setLOS(string $LOS): self
    {
        $this->LOS = $LOS;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }
    public function setDateAdded(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }
    
    public function getUserAdded(): ?User
    {
        return $this->User_added;
    }

    public function setUserAdded(?User $User_added): self
    {
        $this->User_added = $User_added;

        return $this;
    }

    public function getObjectState(): ?string
    {
        return $this->ObjectState;
    }

    public function setObjectState(string $ObjectState): self
    {
        $this->ObjectState = $ObjectState;

        return $this;
    }
}
