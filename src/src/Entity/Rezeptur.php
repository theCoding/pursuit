<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RezepturRepository")
 */
class Rezeptur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Bezeichnung;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Beschreibung;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Notiz;

    /**
     * @ORM\Column(type="float")
     */
    private $Alkoholgehalt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="rezepturs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User_added;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_added;

    /**
     * @ORM\Column(type="array")
     */
    private $Zutaten = [];

    /**
     * @ORM\Column(type="array")
     */
    private $Menge = [];

    /**
     * @ORM\Column(type="array")
     */
    private $Lieferant = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produkt", mappedBy="Rezept")
     */
    private $produkts;

    public function __construct()
    {
        $this->produkts = new ArrayCollection();
        $this->ansatzs = new ArrayCollection();
       
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBezeichnung(): ?string
    {
        return $this->Bezeichnung;
    }

    public function setBezeichnung(string $Bezeichnung): self
    {
        $this->Bezeichnung = $Bezeichnung;

        return $this;
    }

    public function getBeschreibung(): ?string
    {
        return $this->Beschreibung;
    }

    public function setBeschreibung(string $Beschreibung): self
    {
        $this->Beschreibung = $Beschreibung;

        return $this;
    }

    public function getNotiz(): ?string
    {
        return $this->Notiz;
    }

    public function setNotiz(string $Notiz): self
    {
        $this->Notiz = $Notiz;

        return $this;
    }

    public function getAlkoholgehalt(): ?float
    {
        return $this->Alkoholgehalt;
    }

    public function setAlkoholgehalt(float $Alkoholgehalt): self
    {
        $this->Alkoholgehalt = $Alkoholgehalt;

        return $this;
    }

    public function getUserAdded(): ?User
    {
        return $this->User_added;
    }

    public function setUserAdded(?User $User_added): self
    {
        $this->User_added = $User_added;

        return $this;
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->date_added;
    }

    public function setDateAdded(\DateTimeInterface $date_added): self
    {
        $this->date_added = $date_added;

        return $this;
    }

    public function getZutaten(): ?array
    {
        return $this->Zutaten;
    }

    public function setZutaten(array $Zutaten): self
    {
        $this->Zutaten = $Zutaten;

        return $this;
    }
    public function addZutaten($Zutat): self
    {
        array_push($this->Zutaten, $Zutat);
        //$this->Zutaten = $this->Zutaten + $Zutat;
        return $this;
    }

    public function getMenge(): ?array
    {
        return $this->Menge;
    }

    public function setMenge(array $Menge): self
    {
        $this->Menge = $Menge;

        return $this;
    }
    public function addMenge($Menge): self
    {
        array_push($this->Menge, $Menge);
        //$this->Menge = $this->Menge + $Menge;
        return $this;
    }

    public function getLieferant(): ?array
    {
        return $this->Lieferant;
    }

    public function setLieferant(array $Lieferant): self
    {
        $this->Lieferant = $Lieferant;

        return $this;
    }
    public function addLieferant($Lieferant): self
    {
        array_push($this->Lieferant, $Lieferant);
        //$this->Lieferant = $this->Lieferant + $Lieferant;
        return $this;
    }

    /**
     * @return Collection|Produkt[]
     */
    public function getProdukts(): Collection
    {
        return $this->produkts;
    }

    public function addProdukt(Produkt $produkt): self
    {
        if (!$this->produkts->contains($produkt)) {
            $this->produkts[] = $produkt;
            $produkt->setRezept($this);
        }

        return $this;
    }

    public function removeProdukt(Produkt $produkt): self
    {
        if ($this->produkts->contains($produkt)) {
            $this->produkts->removeElement($produkt);
            // set the owning side to null (unless already changed)
            if ($produkt->getRezept() === $this) {
                $produkt->setRezept(null);
            }
        }

        return $this;
    }

    private $ZutatString = "";

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ansatz", mappedBy="Rezept")
     */
    private $ansatzs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ObjectState;
    public function setZutatString(String $zutat){
        $this->ZutatString = $zutat;
        return $this;
    }
    public function getZutatString(){
        return $this->ZutatString;
    }
    public function makeZutatString($zutat, $lieferant){
        if($this->ZutatString == ""){

        }else{
            //=[3,2,4],[2,5,2]
            $strings = explode("[", $this->ZutatString); //=3,2,4],     2,5,2]
            for($i = 1; $i < sizeof($strings); $i++){
                $strings[$i] = explode("]", $strings[$i]);
                $ergebnis = explode(",", $strings[$i][0]); //=3     2   4
                dump($ergebnis);
                dump($lieferant);
                $this->addLieferant($lieferant[$ergebnis[2]-1]);
                $this->addMenge($ergebnis[1]);
                $this->addZutaten($zutat[$ergebnis[2]-1]);
            }
            
            //$this->addGruppe($gruppeArray[$this->GruppeInt]);
        }
       
    }

    /**
     * @return Collection|Ansatz[]
     */
    public function getAnsatzs(): Collection
    {
        return $this->ansatzs;
    }

    public function addAnsatz(Ansatz $ansatz): self
    {
        if (!$this->ansatzs->contains($ansatz)) {
            $this->ansatzs[] = $ansatz;
            $ansatz->setRezept($this);
        }

        return $this;
    }

    public function removeAnsatz(Ansatz $ansatz): self
    {
        if ($this->ansatzs->contains($ansatz)) {
            $this->ansatzs->removeElement($ansatz);
            // set the owning side to null (unless already changed)
            if ($ansatz->getRezept() === $this) {
                $ansatz->setRezept(null);
            }
        }

        return $this;
    }

    public function getObjectState(): ?string
    {
        return $this->ObjectState;
    }

    public function setObjectState(string $ObjectState): self
    {
        $this->ObjectState = $ObjectState;

        return $this;
    }
}
