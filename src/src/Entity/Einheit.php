<?php

namespace App\Entity;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Traits\CommonTrait;
use App\Entity\Traits\DateAndUserAddedTrait;
use App\Entity\Traits\FieldNameTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EinheitRepository")
 */
class Einheit extends AbstractEntity {


    use FieldNameTrait;
    use DateAndUserAddedTrait;

    const TITLE_FOR_COLUMN_NAME = "Name";


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $Name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $ObjectState;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $DateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $UserAdded;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $DateEdit;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $EditorId;


    public function getFieldListWithTitles() {
        $result                              = [];
        $result[self::TITLE_FOR_COLUMN_NAME] = $this->Name;

        return $result;
    }
}
