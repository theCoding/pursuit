<?php
/*
Übersichts Controller.
Hier wird das "Dashboard" generiert (l 746)
Und auch die übersicht der einzelnen Items (sowie die Einzelansicht dieser Items) (l 100)

Der Login ist ab l 37 implementiert
*/

namespace App\Controller;

use App\Controller\Traits\AuthorizationTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\WarengruppeFormType;
use App\Form\LieferantFormType;
use App\Entity\Warengruppe;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Zutat;
use App\Entity\Wareneingang;
use App\Entity\Rezeptur;
use App\Entity\Ansatz;
use App\Entity\Einheit;
use App\Entity\Produkt;
use App\Entity\Abfullung;
use App\Entity\Updates;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminPanelController extends AbstractController {
    use AuthorizationTrait;
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils) {
        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        // dump($error);
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'pdf'           => 0,
        ]);
    }



    //Alte funktionen für das alte Panel [start]

    /**
     * @Route("/admin/panel", name="admin_panel")
     */
    public function index() {

        $repository   = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten  = $repository->findAll();
        $repository   = $this->getDoctrine()->getRepository(User::class);
        $user         = $repository->findAll();
        $repository   = $this->getDoctrine()->getRepository(Wareneingang::class);
        $wareneingang = $repository->findAll();
        $repository   = $this->getDoctrine()->getRepository(Zutat::class);
        $zutat        = $repository->findAll();
        $repository   = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppe  = $repository->findAll();
        $repository   = $this->getDoctrine()->getRepository(Rezeptur::class);
        $rezeptur     = $repository->findAll();

        return $this->render('adminpanel.html.twig', [
            'var'          => 'empty',
            'users'        => $user,
            'lieferanten'  => $lieferanten,
            'zutaten'      => $zutat,
            'gruppen'      => $warengruppe,
            'wareneingang' => $wareneingang,
            'rezeptur'     => $rezeptur,
        ]);
    }

    /**
     * @Route("/admin/create", name="creator")
     */
    public function creator(Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $manager = $this->getDoctrine()->getManager();
        $user    = new User();
        $user->setUsername("2306");
        $user->setPassword($passwordEncoder->encodePassword($user, "thesecurityisonlybullshit"));
        $user->setDisplayName("Simon Stasius");
        $user->setRoles(['ROLE_ADMIN']);
        $user->setSignature("none");
        $manager->persist($user);
        $manager->flush();

        return $this->render('adminpanel.html.twig', [
            'var'         => 'empty',
            'users'       => null,
            'lieferanten' => null,
            'zutaten'     => null,
            'gruppen'     => null,
        ]);
    }
    //Alte funktionen für das alte Panel [end]

    /**
     * @Route("/panel/{pItem}", name="lieferanten_list")
     */
    public function panel($pItem, Request $request) {
        //Daten, die immer gleich sind werden aus DB oder HTTP Request geholt, sonstige variablen auf NULL gesetzt.
        $username = $this->getUser();
        //dump($user);
        $pName      = "";
        $pInternal  = $pItem;
        $ids        = "";
        $link       = "";
        $info_array = "";
        $headings   = "";
        $state      = [];
        $archiv     = $request->query->get('archiv');
        $pdf        = 0;
        $get        = $request->query->get('get');
        $names      = [];
        if ($archiv == "set") {
            /*Wenn die Variable "archiv im Request = "set" ist, erkennt das System, dass es sich um einen Link, nach der Editierung eines 
            Items handelt. Das Item, welches vor dem anzeigen der aktuellen Seite bearbeitet wurde, wird in der Datenbank herausgesucht,
            der Status wird geändert (wenn es vorher gespeichert und veröffentlich war wird der status auf archiviert gesetzt, sonst auf 
            gespeichert und veröffentlicht) und das Item geht zurück in die DB
            */
            $repo      = $this->getDoctrine()->getRepository('App\\Entity\\' . $pItem);
            $manager   = $this->getDoctrine()->getManager();
            $obj_id    = $request->query->get('id');
            $obj       = $repo->find($obj_id);
            $old_state = $obj->getObjectState();
            if ($old_state == 2) {
                $obj->setObjectState(3);
            } else {
                $obj->setObjectState(2);
            }

            $manager->persist($obj);
            $manager->flush();
        }
        if ($pItem == "Lieferant") {

            return $this->redirectToRoute("LieferantList");

        } else if ($pItem == "Zutat") {

            return $this->redirectToRoute("ZutatList");
        } else if ($pItem == "Rezeptur") {
            $pName      = "Rezepturen";
            $pInternal  = "Rezeptur";
            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte    = $rezeptrepo->findAll();


            //($rezepte);
            $headings   = ["Bezeichnung", "Beschreibung", "Notiz", "Alkoholgehalt", "Zutaten", "Produkte", "Hinzugefüg am", "Angelegt von"];
            $link       = ["stats", "", "", "", "stats", "stats", "", ""];
            $info_array = [];
            $ids        = [];
            if ($rezepte[0] != null) {
                foreach ($rezepte as $rezept) {
                    //dump($rezept);
                    $objstate = $rezept->getObjectState();
                    $zutaten  = $rezept->getZutaten();
                    if (sizeof($zutaten) > 0) {
                        $zutatenstring = $zutaten[0]->getName();
                    } else {
                        $zutatenstring = "";
                    }
                    for ($i = 1; $i < sizeof($zutaten); $i++) {
                        $zutatenstring = $zutatenstring . ", " . $zutaten[$i]->getName();
                    }

                    $produkte = $rezept->getProdukts();

                    $produktstring = "0";
                    if (sizeof($produkte) > 0) {
                        $produktstring = $produkte[0]->getBezeichnung();
                    } else {
                        $produktstring = "0";
                    }
                    for ($i = 1; $i < sizeof($produkte); $i++) {
                        $produktstring = $produktstring . ", " . $produkte[$i]->getBezeichnung();
                    }


                    $items = [];
                    array_push($items, $rezept->getBezeichnung());
                    array_push($items, $rezept->getBeschreibung());
                    array_push($items, $rezept->getNotiz());
                    array_push($items, $rezept->getAlkoholgehalt());
                    array_push($items, $zutatenstring);
                    array_push($items, $produktstring);
                    array_push($items, $rezept->getDateAdded()->format('Y-m-d H:i:s'));
                    array_push($items, $rezept->getUserAdded()->getDisplayName());

                    array_push($ids, $rezept->getId());
                    array_push($state, $objstate);
                    array_push($info_array, $items);
                    $theID = $rezept->getId();
                }
            }
        } else if ($pItem == "Warengruppe") {

            return $this->redirectToRoute("WarengruppeList");

        } else if ($pItem == "Wareneingang") {

            return $this->redirectToRoute("WareneingangList");

        } else if ($pItem == "Produkt") {
            $pName       = "Produkte";
            $pInternal   = "Produkt";
            $produktrepo = $this->getDoctrine()->getRepository(Produkt::class);
            $produkte    = $produktrepo->findAll();


            $headings   = ["Bezeichnung", "Rezept", "Kunde", "Hinzugefüg am", "Angelegt von"];
            $link       = ["", "", "", "stats", "", ""];
            $info_array = [];
            $ids        = [];
            if ($produkte[0] != null) {
                foreach ($produkte as $produkt) {
                    $objstate = $lieferant->getObjectState();


                    /*$ansatze = $produkt->getAnstzs();
                    if(sizeof($ansatze)>0){
                        $ansatzstring = $ansatze[0]->getDatum()->format('Y-m-d H:i:s');
                    }else{
                        $ansatzstring = "";
                    }
                    for($i = 1; $i < sizeof($ansatze); $i++){
                       $ansatzstring = $ansatzstring . ", " . $ansatze[$i]->getDatum()->format('Y-m-d H:i:s');
                    }*/

                    $items = [];
                    array_push($items, $produkt->getBezeichnung());
                    array_push($items, $produkt->getRezept()->getBezeichnung());
                    array_push($items, $produkt->getKunde());
                    //array_push($items, $ansatzstring);


                    array_push($items, $produkt->getDateAdded()->format('Y-m-d H:i:s'));
                    array_push($items, $produkt->getUserAdded()->getDisplayName());

                    array_push($ids, $produkt->getId());
                    array_push($state, $objstate);
                    array_push($info_array, $items);
                    $theID = $produkt->getId();
                }
            }
        } else if ($pItem == "Ansatz") {
            $pName      = "Ansatz";
            $pInternal  = "Ansatz";
            $ansatzrepo = $this->getDoctrine()->getRepository(Ansatz::class);
            $ansatze    = $ansatzrepo->findAll();


            $headings   = ["Rezept", "Status", "Datum", "Abfüllungen", "Erzeugt von"];
            $link       = ["", "", "", "", "", ""];
            $info_array = [];
            $ids        = [];
            if ($ansatze[0] != null) {
                foreach ($ansatze as $ansatz) {
                    $objstate = $ansatz->getObjectState();


                    $abfullungen = $ansatz->getAbfullungs();
                    if (sizeof($abfullungen) > 0) {
                        $abfullungstring = $abfullungen[0]->getDate()->format('Y-m-d H:i:s');
                    } else {
                        $abfullungstring = "";
                    }
                    for ($i = 1; $i < sizeof($abfullungen); $i++) {
                        $abfullungstring = $abfullungstring . ", " . $abfullungen[$i]->getDate()->format('Y-m-d H:i:s');
                    }

                    $items = [];
                    array_push($items, $ansatz->getRezept()->getBezeichnung());
                    array_push($items, $ansatz->getStatusString());
                    array_push($items, $ansatz->getDatum()->format('Y-m-d H:i:s'));
                    array_push($items, $abfullungstring);


                    array_push($items, $ansatz->getUserAdded()->getDisplayName());

                    array_push($ids, $ansatz->getId());
                    array_push($state, $objstate);
                    array_push($info_array, $items);
                    $theID = $ansatz->getId();
                }
            }
        } else if ($pItem == "Abfullung") {
            $pName       = "Abfüllung";
            $pInternal   = "Abfullung";
            $abfullrepo  = $this->getDoctrine()->getRepository(Abfullung::class);
            $abfullungen = $abfullrepo->findAll();


            $headings   = ["Datum", "Ansatz", "Produkt", "Verbrauch Ansatz", "Menge der Flaschen", "Flaschen", "LOS", "Benutzer"];
            $link       = ["", "", "", "", "", "", "", ""];
            $info_array = [];
            $ids        = [];
            if ($abfullungen[0] != null) {
                foreach ($abfullungen as $abfullung) {
                    $objstate = $abfullung->getObjectState();


                    /*$abfullungen = $ansatz->getAbfullungs();
                    if(sizeof($abfullungen)>0){
                        $abfullungstring = $abfullungen[0]->getDatum();
                    }else{
                        $abfullungstring = "";
                    }
                    for($i = 1; $i < sizeof($abfullungen); $i++){
                       $abfullungstring = $abfullungstring . ", " . $abfullungen[$i]->getDatum();
                    }*/

                    $items = [];
                    array_push($items, $abfullung->getDate()->format('Y-m-d H:i:s'));
                    array_push($items, "Ansatz von Rezept " . $abfullung->getAnsatz()->getRezept()->getBezeichnung());
                    array_push($items, $abfullung->getProdukt()->getBezeichnung());
                    array_push($items, $abfullung->getVerbrauchAnsatz());
                    array_push($items, $abfullung->getMengeFlaschen());
                    array_push($items, $abfullung->getFlaschen());
                    array_push($items, $abfullung->getLOS());


                    array_push($items, $abfullung->getUserAdded()->getDisplayName());

                    array_push($ids, $abfullung->getId());
                    array_push($state, $objstate);
                    array_push($info_array, $items);
                    $theID = $abfullung->getId();
                }
            }
        } else if ($pItem == "Einheit") {

            return $this->redirectToRoute("EinheitList");
        } else if ($pItem == "User") {

            return $this->redirectToRoute("UserList");

        } else if ($pItem == "System") {
            $info_array   = [];
            $updaterepo   = $this->getDoctrine()->getRepository(Updates::class);
            $updates      = $updaterepo->findAll();
            $update_array = [];
            $headings     = [];
            $updates      = array_reverse($updates);
            foreach ($updates as $update) {
                $update_txt = ($update->getAvailable()) ? "Es wurde ein Update durchgeführt" : "Es wurde nach einem Update gesucht";
                array_push($headings, $update_txt);
                $update_txt = "am " . $update->getDate()->format('Y-m-d H:i:s') . " in der Version " . $update->getNumber() . " mit folgenden Änderungen: " . $update->getContent();
                array_push($update_array, $update_txt);
            }
            array_push($info_array, $update_array);


            return $this->render('system_info.html.twig', [
                'name'        => $pName,
                'headings'    => $headings,
                'elements'    => $info_array,
                'links'       => $link,
                'id'          => $ids,
                'intern_name' => $pName,
                'state'       => $state,
                'user'        => $username->getDisplayName(),
                'user_role'   => $username->getRoles()[0] == "ROLE_ADMIN" ? 1 : 0,
                'pdf'         => $pdf,
                'names'       => $names,
                'get_var'     => $get,
                'single_id'   => 0,
            ]);
        }

        return $this->render('basic_template.html.twig', [
            'name'        => $pName,
            'headings'    => $headings,
            'elements'    => $info_array,
            'links'       => $link,
            'id'          => $ids,
            'intern_name' => $pInternal,
            'state'       => $state,
            'user'        => $username->getDisplayName(),
            'user_role'   => $username->getRoles()[0] == "ROLE_ADMIN" ? 1 : 0,
            'pdf'         => $pdf,
            'names'       => $names,
            'get_var'     => $get,
            'single_id'   => $theID,
        ]);
    }

    /**
     * @Route("/", name="mainpage")
     */
    public function mainpage() {
        return new Response(
            '<script>
            window.location = "/panel";
            </script>'
        );
    }

    /**
     * @Route("/panel", name="dashboard")
     */
    public function dashboard(Request $request) {
        $username = $this->getUser();
        $archiv   = $request->query->get('archiv');

//        dump($archiv);
        $role = $this->generateRoleCode($username);
        return $this->render('dashboard.html.twig', [
            'intern_name' => "",
            'name'        => "",
            'user'        => $username->getDisplayName(),
            'pdf'         => 0,
            'user_role'   => $role,
        ]);
    }

}
