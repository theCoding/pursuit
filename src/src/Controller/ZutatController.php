<?php

namespace App\Controller;

use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Einheit;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Wareneingang;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\ZutatFormType;
use App\Repository\ZutatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ZutatController extends AbstractController {

    use EditTrait;
    use AuthorizationTrait;

    /**
     * @Route("/admin/Zutat/add", name="ZutatAdd")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $manager     = $this->getDoctrine()->getManager();
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";

        /**
         * @TODO
         * Logik mit Zugriffsrechten lässt sich besser in einen
         * benutzerdefinierten abstrakten Controller einfügen.
         *
         */
        $role = $this->generateRoleCode($username);


        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->getAllForHerstellerOrLieferant(Lieferant::IS_LIEFERANT);

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }

        $warengrupperepo   = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppen      = $warengrupperepo->findAll();
        $warengruppe_array = [];
        foreach ($warengruppen as $key => $warengruppe) {
            if ($warengruppe->getObjectState() != 3) {
                $warengruppe_array[$warengruppe->getName()] = $key;
            }
        }
        $warengruppe_array["-"] = -1;

        $einheitrepo     = $this->getDoctrine()->getRepository(Einheit::class);
        $einheiten       = $einheitrepo->findAll();
        $einheiten_array = [];


        foreach ($einheiten as $key => $einheit) {
            $einheiten_array[$einheit->getName()] = $einheit->getName();
        }

        $form_options = [
            'lieferanten' => $lieferanten_array,
            'warengruppe' => $warengruppe_array,
            'einheit'     => $einheiten_array,
        ];

        $item = new Zutat();
        $form = $this->createForm(
            ZutatFormType::class,
            $item,
            $form_options
        );

        $form->handleRequest($request);


        $save        = false;
        $permissions = true;
        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();


            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(Zutat::STATE_ENABLED);


            $item->makeLieferInt($lieferanten);
            $item->makeGruppeInt($warengruppen);
            $item->setMenge(0);


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "ZutatList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }

        return $this->render('zutat/add.twig', [
            'form'             => $form->createView(),
            'name'             => "Zutat",
            'item'             => "Zutat",
            'intern_name'      => "Zutat",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'einheitenCount'   => count($einheiten),
        ]);
    }


    /**
     * @Route("/admin/Zutat/delete/{id}", name="ZutatDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $manager   = $this->getDoctrine()->getManager();
        $ZutatRepo = $this->getDoctrine()->getRepository(Zutat::class);

        $username = $this->getUser();
        $isAdmin = $username->getRoles()[0] == "ROLE_ADMIN" ;
        if(! $isAdmin){
            return $this->redirectToRoute('ZutatList');
        }

        /**
         * @var Zutat $zutat
         * @var Zutat $newZutat
         */
        $zutat = $ZutatRepo->find($id);

        if(count($ZutatRepo->getCountEnabled()) == 1){


            Zutat::Remove($zutat);

            $manager->flush();

            return $this->redirectToRoute('ZutatList');
        }

        $newId    = $request->request->get("newElement", -1);
        $newZutat = $ZutatRepo->find((int) $newId);
        if (! $newZutat) {
            throw new \Exception("New Zutat not found");
        }

        $Wareneingangs = $zutat->getWareneingangs();
        $Warengruppen = $zutat->getGruppe();
        $Lieferats     = $zutat->getLieferant();

        foreach ($Wareneingangs as $Wareneingang) {
            $Wareneingang->setZutat($newZutat);
        }

        foreach ($Warengruppen as $Warengruppe) {
            $Warengruppe->removeZutat($zutat);
            $Warengruppe->addZutat($newZutat);
        }

        foreach ($Lieferats as $Lieferat) {
            $Lieferat->removeZutat($zutat);
            $Lieferat->addZutat($newZutat);
        }

        Zutat::Remove($zutat);

        $manager->flush();

        return $this->redirectToRoute('ZutatList');
    }


    /**
     * @Route("/admin/Zutat/list/", name="ZutatList")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request) {
        $username = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);


        $state    = [];
        $archiv   = $request->query->get('archiv', false);
        $archiv   = trim($archiv) === "true";
        $pdf      = 0;
        $get      = $request->query->get('get');
        $names    = [];

        $pName     = "Zutat";
        $pInternal = "Zutat";
        $repo      = $this->getDoctrine()->getRepository(Zutat::class);
        $zutaten   = $repo->findAllWithArchive((bool) $archiv);
        $currentZutat = false;
        $ids = [];
        if (count($zutaten) > 0) {
            $ids      = array_keys($zutaten);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentZutat = $zutaten[$curentId];
        }

        return $this->render('zutat/list.twig', [
            'zutaten'      => $zutaten,
            'name'         => $pName,
            'id'           => $ids,
            'intern_name'  => $pInternal,
            'state'        => $state,
            'user'         => $username->getDisplayName(),
            'user_role'    => $role,
            'pdf'          => $pdf,
            'names'        => $names,
            'get_var'      => $get,
            'currentZutat' => $currentZutat,
        ]);
    }

    /**
     * @Route("/admin/Zutat/archive/{id}", name="ZutatArchive")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function archive($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $ZutatRepo = $this->getDoctrine()->getRepository(Zutat::class);
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        /**
         * @var Zutat $Zutat
         */
        $Zutat = $ZutatRepo->find($id);

        Zutat::Archive($Zutat);

        $manager->flush();

        return $this->redirectToRoute('ZutatList');
    }


    /**
     * @Route("/admin/Zutat/edit/{id}", name="ZutatEdit")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $repo = $this->getDoctrine()->getRepository(Zutat::class);
        $item = $repo->find($id);
        $username    = $this->getUser();
        $manager     = $this->getDoctrine()->getManager();

        if(! $this->checkEditAccess($username, $item)){
            return $this->redirectToRoute('ZutatList');
        }
        $manager->flush();


        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";

        /**
         * @TODO
         * Logik mit Zugriffsrechten lässt sich besser in einen
         * benutzerdefinierten abstrakten Controller einfügen.
         *
         */
        $role = $this->generateRoleCode($username);


        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->getAllForHerstellerOrLieferant(Lieferant::IS_LIEFERANT);

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }

        $warengrupperepo   = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppen      = $warengrupperepo->findAll();
        $warengruppe_array = [];
        foreach ($warengruppen as $key => $warengruppe) {
            if ($warengruppe->getObjectState() != 3) {
                $warengruppe_array[$warengruppe->getName()] = $key;
            }
        }
        $warengruppe_array["-"] = -1;

        $einheitrepo     = $this->getDoctrine()->getRepository(Einheit::class);
        $einheiten       = $einheitrepo->findAll();
        $einheiten_array = [];


        foreach ($einheiten as $key => $einheit) {
            $einheiten_array[$einheit->getName()] = $einheit->getName();
        }

        $form_options = [
            'lieferanten' => $lieferanten_array,
            'warengruppe' => $warengruppe_array,
            'einheit'     => $einheiten_array,
        ];


        $form = $this->createForm(
            ZutatFormType::class,
            $item,
            $form_options
        );

        $form->handleRequest($request);


        $save        = false;
        $permissions = true;
        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->makeLieferInt($lieferanten);
            $item->makeGruppeInt($warengruppen);


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "ZutatList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }

        return $this->render('zutat/edit.twig', [
            'form'             => $form->createView(),
            'name'             => "Zutat",
            'item'             => "Zutat",
            'intern_name'      => "Zutat",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'einheitenCount'   => count($einheiten),
            'id'               => $id,
        ]);

    }

    /**
     * @Route("/admin/Zutat/edit_check}", name="ZutatEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck($request){
        return $this->editStateCheckForRequest($request, Zutat::class);
    }

    /**
     * @Route("/admin/Zutat/edit_update", name="ZutatEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateUpdateForRequest($request, Zutat::class);
    }

}