<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Warengruppe;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Zutat;
use App\Entity\Wareneingang;
use App\Entity\Rezeptur;
use App\Entity\Ansatz;
use App\Entity\Produkt;
use App\Entity\Abfullung;
use App\Entity\Einheit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AdminEditController extends AbstractController
{
    /**
     * @Route("/admin/edit", name="admin_edit")
     */
    public function index()
    {
        return $this->render('admin_add/index.html.twig', [
            'controller_name' => 'AdminAddController',
        ]);
    }
     /**
     * @Route("/admin/edit/{pItem}", name="edit")
     */
    public function edit($pItem, Request $request)
    {
        $username = $this->getUser();
        $ZutatString = "";
        $e1effect = "";
        $e1is = "";
        $ae1 = [];
            $ae2 = [];
        $lieferanten = "";
        $zutaten = "";
        $repository = $this->getDoctrine()->getRepository(User::class);
            $manager = $this->getDoctrine()->getManager();
            $class = new \ReflectionClass("App\Entity\\" . $pItem);
            $item = $class->newInstanceArgs();
           
            $obj_id = $request->query->get('id');

            //Set State
           /* $repo = $this->getDoctrine()->getRepository('App\\Entity\\'.$pItem);
            $obj = $repo->find($obj_id);
            $obj->setObjectState(1);
            dump($obj);
            $manager->persist($obj);
            $manager->flush();*/
            
        if($pItem == "Lieferant"){

           return $this->redirectToRoute('LieferantEdit',['id'=>$obj_id]);

        }else if($pItem == "Zutat"){
            return $this->redirectToRoute('ZutatEdit',['id'=>$obj_id]);

        }else if($pItem == "Warengruppe"){

            return $this->redirectToRoute('WarengruppeEdit',['id'=>$obj_id]);

        }else if($pItem == "Wareneingang"){

            return $this->redirectToRoute('WareneingangEdit', ['id'=>$obj_id]);

        }else if($pItem == "Einheit"){

            return $this->redirectToRoute('EinheitEdit', ['id'=>$obj_id]);

        }else if($pItem == "User"){

            return $this->redirectToRoute('UserEdit', ['id'=>$obj_id]);

        }else if($pItem == "Rezeptur"){
            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte = $rezeptrepo->find($obj_id);

            $lieferantrepo = $this->getDoctrine()->getRepository(Lieferant::class);
            $lieferanten = $lieferantrepo->findAll();
            
            

            $zutatenrepo = $this->getDoctrine()->getRepository(Zutat::class);
            $zutaten = $zutatenrepo->findAll();
            

            $lieferanten = $lieferantrepo->findAll();
            $zutaten = $zutatenrepo->findAll();

            $lieferanten_array = [];
            foreach($lieferanten as $key=>$lieferant){
    
                $lieferanten_array = $lieferanten_array + [$lieferant->getAlias() => $key];
            }
            $zutaten_array = [];
            foreach($zutaten as $key=>$zutat){
                $zutaten_array = $zutaten_array + [$zutat->getName() => $key];
            }

           $mengen = $rezepte->getMenge();
           $lieferants = $rezepte->getLieferant();
           //dump($lieferanten);
            foreach($rezepte->getZutaten() as $key=>$zutat){
                $zutatkey = intval($zutaten_array[$zutat->getName()]) + 1;
                $lieferkey = intval($lieferanten_array[$lieferants[$key]->getAlias()]) + 1;
                $ZutatString = $ZutatString . "[" . $zutatkey . "," . $mengen[$key] . "," . $lieferkey . "]";
            }
            //dump($ZutatString);
            $form = $this->createForm('App\Form\\' . $pItem .'FormType'::class, $rezepte);
        }else if($pItem == "Produkt"){
            $produktrepo = $this->getDoctrine()->getRepository(Produkt::class);
            $produkt = $produktrepo->find($obj_id);

            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte = $rezeptrepo->findAll();
            $rezept_array = [];
            foreach($rezepte as $key=>$rezept){
    
                $rezept_array = $rezept_array + [$rezept->getBezeichnung() => $key];
            }
            $form_options = ['rezepte' => $rezept_array];

            $produkt->reconstructRezeptInt($rezept_array);
            $form = $this->createForm('App\Form\\' . $pItem .'FormType'::class, $produkt, $form_options);
        }else if($pItem == "Ansatz"){
            $ansatzrepo = $this->getDoctrine()->getRepository(Ansatz::class);
            $ansatz = $ansatzrepo->find($obj_id);


            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte = $rezeptrepo->findAll();
            $rezept_array = [];
            foreach($rezepte as $key=>$rezept){
    
                $rezept_array = $rezept_array + [$rezept->getBezeichnung() => $key];
            }
            $form_options = ['rezept' => $rezept_array];

            $ansatz->reconstructRezeptInt($rezept_array);
            $form = $this->createForm('App\Form\\' . $pItem .'FormType'::class, $ansatz, $form_options);
        }else if($pItem == "Abfullung"){
            $abfullungrepo = $this->getDoctrine()->getRepository(Abfullung::class);
            $abfullung = $abfullungrepo->find($obj_id);

            $produktrepo = $this->getDoctrine()->getRepository(Produkt::class);
            $produkte = $produktrepo->findAll();
            $produkt_array = [];
            foreach($produkte as $key=>$produkt){
    
                $produkt_array = $produkt_array + [$produkt->getBezeichnung() => $key];
            }

            $ansatzrepo = $this->getDoctrine()->getRepository(Ansatz::class);
            $ansatze = $ansatzrepo->findAll();
            $ansatz_array = [];
            foreach($ansatze as $key=>$ansatz){
    
                $ansatz_array = $ansatz_array + [$ansatz->getRezept()->getBezeichnung() => $key];
            }

            $form_options = ['produkt' => $produkt_array, 'ansatz' => $ansatz_array];

            $abfullung->reconstructAnsatzInt($ansatz_array);
            $abfullung->reconstructProduktInt($produkt_array);
            $form = $this->createForm('App\Form\\' . $pItem .'FormType'::class, $abfullung, $form_options);
        }else{
            
            
            
            
    
           
           
    
           
        }
       
       
        $form->handleRequest($request);
       // dump($form);
        $save = false;
        if($form->isSubmitted() && $form->isValid()){
            
            $item = $form->getData();
            $item->setDateAdded(new \DateTime('now'));
            $user = $repository->find(1);
            $item->setUserAdded($user);
            $item->setObjectState(2);
            if($pItem == "Lieferant"){
               
            }else if($pItem == "Zutat"){
                $item->makeLieferInt($lieferanten);
                $item->makeGruppeInt($warengruppen);
                $item->setMenge(0);
            }else if($pItem == "Wareneingang"){
               $zutat = $old_eingang->getZutat();
               $menge = $zutat->getMenge();
               $zutat->setMenge($menge - $old_menge);

                $item->makeLieferInt($lieferanten);
                $item->makeZutatInt($zutaten);
                $zutat = $item->getZutat();
                $menge = $zutat->getMenge();
                $zutat->setMenge($menge + $item->getMenge());
              //  $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
               // $zutat->setMenge($zutat->getMenge() + $item->getMenge() - $old_menge);
            }else if($pItem == "Warengruppe"){
               
                $item->makeGruppeInt($warengruppen);
            }else if($pItem == "Rezeptur"){
               
                $item->makeZutatString($zutaten, $lieferanten);

                $lieferanten = $lieferantrepo->findAll();
                $zutaten = $zutatenrepo->findAll();
                
            }else if($pItem == "Produkt"){
                $item->makeRezeptInt($rezepte);
            }else if($pItem == "Ansatz"){
                $item->makeProduktInt($rezepte);
                $zutaten = $item->getRezept()->getZutaten();
                $mengen = $item->getRezept()->getMenge();
                for($i = 0; $i < sizeof($zutaten); $i++){
                    $zutaten[$i]->setMenge($zutaten[$i]->getMenge() - $mengen[$i]);
                    //dump($zutaten[$i]);
                }
            }else if($pItem == "Abfullung"){
                $item->makeAnsatzInt($ansatze);
                
                $item->makeProduktInt($produkte);
                //dump($item);
            }
            
           
           
         
           
            $manager->persist($item);
            $manager->flush();
            $save = true;
        }
        
        return $this->render('add_template.html.twig', [
            'form' => $form->createView(),
            'name' => $pItem,
            'intern_name' => $pItem,
            'item' => $pItem,
            'saved' => $save,
            'lieferanten' => $lieferanten,
            'zutaten' => $zutaten,
            'ausschluss_e1' => $ae1, 
            'ausschluss_e2' => $ae2, 
            'e1is' => $e1is,
            'e1effect' => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id' => $obj_id,
            'user' => $username->getDisplayName(),
            'user_role' => $username->getRoles()[0] == "ROLE_ADMIN" ? 1 : 0,
            'pdf' => '',
        ]);
       
        //return $this->render('adminadd.html.twig');

    }

     /**
     * @Route("/admin/set_unset", name="set_unset")
     */
    public function set_unset(Request $request)
    {
        $obj_id = $request->query->get('id');
        $obj = $request->query->get('item');
        $link  = $request->query->get('link');
        $repo = $this->getDoctrine()->getRepository('App\\Entity\\'.$obj);
        $entity = $repo->find($obj_id);
       if($entity != null){
        $entity->setObjectState(2);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($entity);
            $manager->flush();
       }
       
        return new Response(
            '<html><body><script>window.location = "' . $link . '";</script></body></html>'
        );
    }
}
