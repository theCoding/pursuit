<?php

namespace App\Controller;

use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Abfullung;
use App\Entity\Ansatz;
use App\Entity\Einheit;
use App\Entity\Lieferant;
use App\Entity\Produkt;
use App\Entity\User;
use App\Entity\Wareneingang;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\AbfullungFormType;
use App\Form\EinheitFormType;
use App\Form\LieferantFormType;
use App\Form\WareneingangFormType;
use App\Form\ZutatFormType;
use App\Repository\EinheitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EinheitController extends AbstractController {

    use EditTrait;
    use AuthorizationTrait;
    /**
     * @Route("/admin/Lieferant/add", name="LieferantAdd")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        /**
         * module for all controllers
         */
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $manager     = $this->getDoctrine()->getManager();
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }


        /**
         *  Form create
         */


        $item = new Einheit();

        $form = $this->createForm(EinheitFormType::class, $item);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(2);


            /**
             *  Handler
             */


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "EinheitList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('einheit/add.twig', [
            'form'             => $form->createView(),
            'name'             => "Einheit",
            'item'             => "Einheit",
            'intern_name'      => "Einheit",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
        ]);
    }





    /**
     * @Route("/admin/Einheit/list/", name="EinheitList")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request) {
        $username = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);
        $state    = [];
        $archiv   = $request->query->get('archiv', false);
        $archiv   = trim($archiv) === "true";
        $pdf      = 0;
        $get      = $request->query->get('get');
        $names    = [];

        $pName     = "Einheit";
        $pInternal = "Einheit";
        $repo      = $this->getDoctrine()->getRepository(Einheit::class);
        $repoZutat      = $this->getDoctrine()->getRepository(Zutat::class);
        $einheiten = $repo->findAllWithArchive((bool) $archiv);

        $currentEinheit = false;
        if (count($einheiten) > 0) {
            $ids      = array_keys($einheiten);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentEinheit = $einheiten[$curentId];
        }

        $zutatsAll = [];
        foreach ($einheiten as $einheit) {
            /**
             * @var Einheit $einheit
             */
            $zutats = $repoZutat->getZutatsForEinheit($einheit);
            $zutatsAll[$einheit->getId()] = $zutats;
        }



        return $this->render('einheit/list.twig', [
            'einheiten'      => $einheiten,
            'name'           => $pName,
            'intern_name'    => $pInternal,
            'state'          => $state,
            'user'           => $username->getDisplayName(),
            'user_role'      => $role,
            'pdf'            => $pdf,
            'names'          => $names,
            'get_var'        => $get,
            'currentEinheit' => $currentEinheit,
            'zutats' =>$zutatsAll
        ]);
    }


    /**
     * @Route("/admin/Einheit/delete/{id}", name="EinheitDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function archive($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $manager     = $this->getDoctrine()->getManager();
        $EinheitRepo = $this->getDoctrine()->getRepository(Einheit::class);
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        $einheit     = $EinheitRepo->find($id);

        Einheit::Archive($einheit);

        $manager->flush();
        return $this->redirectToRoute('EinheitList');
    }


    /**
     * @Route("/admin/Einheit/delete/{id}", name="EinheitDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $EinheitRepo = $this->getDoctrine()->getRepository(Einheit::class);
        $ZutatRepo = $this->getDoctrine()->getRepository(Zutat::class);

        $username = $this->getUser();
        $isAdmin = $username->getRoles()[0] == "ROLE_ADMIN" ;
        if(! $isAdmin){
            return $this->redirectToRoute('EinheitList');
        }

        /**
         * @var Einheit $einheit
         * @var Einheit $newEinheit
         */
        $einheit = $EinheitRepo->find($id);

        if(count($EinheitRepo->getCountEnabled()) == 1){

            Einheit::Remove($einheit);

            $manager->flush();

            return $this->redirectToRoute('EinheitList');
        }

        $newId    = $request->request->get("newElement", -1);
        $newEinheit = $EinheitRepo->find((int) $newId);


        if (! $newEinheit) {
            throw new \Exception("New Einheit not found");
        }

        $Zutats = $ZutatRepo->getZutatsForEinheit($einheit);

        foreach ($Zutats as $zutat) {
            /**
             * @var Zutat $zutat
             */
            $zutat->setEinheit($newEinheit->getName());
        }


        Einheit::Remove($einheit);

        $manager->flush();

        return $this->redirectToRoute('EinheitList');
    }



    /**
     * @Route("/admin/Einheit/edit/{id}", name="EinheitEdit")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $username    = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Einheit::class);
        $item = $repo->find($id);
        $manager = $this->getDoctrine()->getManager();
        if(! $this->checkEditAccess($username, $item)){
            return $this->redirectToRoute('LieferantList');
        }
        $manager->flush();
        /**
         * module for all controllers
         */

        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }


        /**
         *  Form create
         */


        $oldName = $item->getName();
        $form = $this->createForm(EinheitFormType::class, $item);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(2);


            /**
             *  Handler
             */


            if ($permissions) {
                $ZutatRepo = $this->getDoctrine()->getRepository(Zutat::class);
                $Zutats = $ZutatRepo->getZutatsForEinheitName($oldName);

                foreach ($Zutats as $zutat) {
                    /**
                     * @var Zutat $zutat
                     */
                    $zutat->setEinheit($item->getName());
                }

                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "EinheitList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('einheit/edit.twig', [
            'form'             => $form->createView(),
            'name'             => "Einheit",
            'item'             => "Einheit",
            'intern_name'      => "Einheit",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'id'               => $id,
        ]);
    }

    /**
     * @Route("/admin/Einheit/edit_check", name="EinheitEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateCheckForRequest($request, Einheit::class);
    }

    /**
     * @Route("/admin/Einheit/edit_update", name="EinheitEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder){

        return $this->editStateUpdateForRequest($request, Einheit::class);
    }
}