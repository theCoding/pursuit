<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Warengruppe;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Zutat;
use App\Entity\Wareneingang;
use App\Entity\Rezeptur;
use App\Entity\Ansatz;
use App\Entity\Produkt;
use App\Entity\Abfullung;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AdminDeleteController extends AbstractController
{
   
     /**
     * @Route("/admin/delete/{pItem}", name="delete")
     */
    public function delete($pItem, Request $request)
    {   
       
        $losch = "";
        $repository = $this->getDoctrine()->getRepository(User::class);
            $manager = $this->getDoctrine()->getManager();
            $class = new \ReflectionClass("App\Entity\\" . $pItem);
            $item = $class->newInstanceArgs();
            $alternative = "";
            $obj_id = $request->query->get('id');
            $delete = 10;
            $uebertrag_id = $request->query->get('uebertragen_id');
        if($pItem == "Lieferant"){

            $lieferantrepo = $this->getDoctrine()->getRepository(Lieferant::class);
            $lieferanten = $lieferantrepo->find($obj_id);
            $alternative_obj = $lieferantrepo->findAll();
            unset($alternative_obj[$obj_id-1]);
            $alternative_obj = array_values($alternative_obj);
            $alternative = [];
            foreach($alternative_obj as $key=>$obj){
                $alternative[$key] = $obj->getAlias();
            }

            $delete = 2;
            if(sizeof($lieferanten->getZutats()) == 0){
                $delete = $delete - 1;
            }
            if(sizeof($lieferanten->getWareneingangs()) == 0){
                $delete = $delete - 1;
            }

            if($uebertrag_id != null){
                $zutats = $lieferanten->getZutats();
                foreach($zutats as $zutat){
                    $zutat = $zutat->removeLieferant($lieferanten);
                    $zutat = $zutat->addLieferant($alternative_obj[$uebertrag_id]);
                    $manager->persist($zutat);  
                }
                $wareneingangs = $lieferanten->getWareneingangs();
                foreach($wareneingangs as $eingang){
                    $eingang = $eingang->setLieferant($alternative_obj[$uebertrag_id]);
                    $manager->persist($eingang);
                }
                $delete = 0;
            }
           
            if($delete == 0){
                $manager->remove($lieferanten);
                $manager->flush();

            }

        }else if($pItem == "Zutat"){
            $losch = "Der rest der vorhandenen Menge wird im Bestand gelöscht";
            $zutatrepo = $this->getDoctrine()->getRepository(Zutat::class);
            $zutaten = $zutatrepo->find($obj_id);
            $alternative_obj = $zutatrepo->findAll();
            unset($alternative_obj[$obj_id-1]);
            $alternative_obj = array_values($alternative_obj);
            $alternative = [];
            foreach($alternative_obj as $key=>$obj){
                $alternative[$key] = $obj->getName();
            }

            $delete = 2;
            if($zutaten->getMenge() == 0){
                $delete = $delete - 1;
            }
            if(sizeof($zutaten->getWareneingangs()) == 0){
                $delete = $delete - 1;
            }
           
            if($uebertrag_id != null){
                $zutaten = $zutaten->setMenge(0);
                

                $wareneingangs = $zutaten->getWareneingangs();
                foreach($wareneingangs as $eingang){
                    $eingang = $eingang->setZutat($alternative_obj[$uebertrag_id]);
                    $alternative_obj[$uebertrag_id]->setMenge($alternative_obj[$uebertrag_id]->getMenge() + $eingang->getMenge());
                    $manager->persist($eingang);
                    $manager->persist($alternative_obj[$uebertrag_id]);
                   
                }
                $delete = 0;
            }
           
            if($delete == 0){
                $manager->remove($zutaten);
                $manager->flush();

            }


        }else if($pItem == "Warengruppe"){
            //$losch = "Der rest der vorhandenen Menge wird im Bestand gelöscht";
            $warengrupperepo = $this->getDoctrine()->getRepository(Warengruppe::class);
            $gruppe = $warengrupperepo->find($obj_id);
            $alternative_obj = $warengrupperepo->findAll();
            unset($alternative_obj[$obj_id-1]);
            $alternative_obj = array_values($alternative_obj);
            $alternative = [];
            foreach($alternative_obj as $key=>$obj){
                $alternative[$key] = $obj->getName();
            }

            $delete = 2;
            if(sizeof($gruppe->getZutaten()) == 0){
                $delete = $delete - 1;
            }
            if(sizeof($gruppe->getWarengruppes()) == 0){
                $delete = $delete - 1;
            }
           
            if($uebertrag_id != null){
              
                

                $zutaten = $gruppe->getZutaten();
                foreach($zutaten as $zutat){
                    $zutat = $zutat->removeGruppe($gruppe);
                    $zutat = $zutat->addGruppe($alternative_obj[$uebertrag_id]);
                    
                    $manager->persist($zutat);
                }
                $warengruppen = $gruppe->getWarengruppes();
                foreach($warengruppen as $untergruppe){
                    $untergruppe = $untergruppe->removeUebergruppe($gruppe);
                    $untergruppe = $untergruppe->addUebergruppe($alternative_obj[$uebertrag_id]);
                    $manager->persist($untergruppe);
                }
                $delete = 0;
            }
           
            if($delete == 0){
                $manager->remove($gruppe);
                $manager->flush();

            }
        }else if($pItem == "Wareneingang"){
            $einganrepo = $this->getDoctrine()->getRepository(Wareneingang::class);
            $eingang = $einganrepo->find($obj_id);
            $menge = $eingang->getMenge();
            $zutat = $eingang->getZutat();
            $zutat_menge = $zutat->getMenge();
            $zutat->setMenge($zutat_menge - $menge);
            $manager->persist($zutat);
            $manager->remove($eingang);
                $manager->flush();
                $delete = 0;
        }else if($pItem == "Rezeptur"){
            
        }else if($pItem == "Produkt"){
            
    
                $produktrepo = $this->getDoctrine()->getRepository(Produkt::class);
                $produkt = $produktrepo->find($obj_id);
                $alternative_obj = $produktrepo->findAll();
                unset($alternative_obj[$obj_id-1]);
                $alternative_obj = array_values($alternative_obj);
                $alternative = [];
                foreach($alternative_obj as $key=>$obj){
                    if($obj->getRezept() == $produkt->getRezept()){
                        $alternative[$key] = $obj->getBezeichnung();
                    }
                    
                }
    
                $delete = 1;
                if(sizeof($produkt->getAbfullungs()) == 0){
                    $delete = $delete - 1;
                }
                
    
                if($uebertrag_id != null){
                    $abfullungs = $produkt->getAbfullungs();
                    foreach($abfullungs as $abfullung){
                        $abfullung = $abfullung->setProdukt($alternative_obj[$uebertrag_id]);
                      
                        $manager->persist($abfullung);  
                    }
                    
                    $delete = 0;
                }
               
                if($delete == 0){
                    $manager->remove($produkt);
                    $manager->flush();
    
                }
        }else if($pItem == "Ansatz"){
            
        }else if($pItem == "Abfullung"){
           
        }else{
            
            
            
            
    
           
           
    
           
        }
       
       
      
        
        return $this->render('admin_delete/index.html.twig', [
           
            'item' => $pItem,
            'delete' => $delete,
            'alternative' => $alternative,
            'id' => $obj_id,
            'losch' => $losch,
        ]);
       
        

    }
}
