<?php
/*
Hallo Manuel
*/
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use App\Entity\Updates;
use App\Entity\Einheit;
use Doctrine\ORM\EntityManagerInterface;

class UpdateController extends AbstractController
{
    /**
     * @Route("/update", name="update")
     */
    public function index()
    {
        $root_dir = $this->container->get('request_stack')->getCurrentRequest()->server->get('DOCUMENT_ROOT');
        $repository = $this->getDoctrine()->getRepository(Updates::class);
        
        $manager = $this->getDoctrine()->getManager();
        
        $client = HttpClient::create();
        
        $response = $client->request('GET', 'http://showroom.thecoding.de/update/paehler/update');

        $content = $response->toArray();
        if($response->getStatusCode() == 200){
            $content = $content["Updates"];
            foreach($content as $update){
                $old_updates = $repository->findOneBy(['Number' => $update['Version']]);
                
                if(!isset($old_updates)){   //Wenn in Datenbank steht, dass Update noch nicht ausgeführt wurde
                    //Update zur Datenbank hinzufügen
                    $files = $update["Files"];
                    foreach($files as $file){
                        $filesystem = new Filesystem();
                        if(!$filesystem->exists($root_dir . '/../tmp')){
                            $filesystem->mkdir($root_dir . '/../tmp');
                        }
                        $response = $client->request('GET', 'http://showroom.thecoding.de/update/paehler/' . $file["ServerName"]);
                        $file_content = $response->getContent();

                        
                        try {
                            $filesystem->dumpFile($root_dir . '/../tmp/' . $file["LocalName"], $file_content);
                        } catch (IOExceptionInterface $exception) {
                            echo "An error occurred while creating your directory at ".$exception->getPath();
                        }

                        $new_dir = str_replace("{DIR}/", "", $file["LocalPath"]);
                        $filesystem->copy($root_dir . '/../tmp/' . $file["LocalName"], $root_dir . '/../' . $new_dir . $file["LocalName"], true);
                        $filesystem->remove($root_dir . '/../tmp/' . $file["LocalName"]);
                        
                    }
                   
                    $db_update = new Updates();
                    $db_update->setNumber($update['Version']);
                    $db_update->setAvailable(true);
                    $db_update->setContent($update["Content"]);
                    $date = date_create_from_format('j-m-Y H:i', $update["Datum"]);
                    $db_update->setDate($date);
                    $manager->persist($db_update);
                    $manager->flush();
                    return new Response("Update Erfolgreich");
                }
            }

            /*$response = $client->request('POST', 'http://showroom.thecoding.de/update/paehler/backup', [
                
                'body' => fopen('var/database.db', 'r'),
            ]);
            return new Response("Database Backup");*/
        }
       
        return new Response("Kein Update vorhanden");
        
    }

    
}

