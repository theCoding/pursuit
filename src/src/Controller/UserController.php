<?php

namespace App\Controller;


use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\UserFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController {

    use EditTrait;
    use AuthorizationTrait;

    /**
     * @Route("/admin/Lieferant/add", name="LieferantAdd")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        /**
         * module for all controllers
         */
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $manager     = $this->getDoctrine()->getManager();
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }
        /**
         *
         */
        $item = new User();
        $form = $this->createForm(UserFormType::class, $item);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setPassword($passwordEncoder->encodePassword($item, "thesecurityisonlybullshit"));
            $item->makeAdmin();
            $item->makeInventoryAuthorized();
            $item->setObjectState(User::STATE_ENABLED);

            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "UserList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('user/add.twig', [
            'form'             => $form->createView(),
            'name'             => "User",
            'item'             => "User",
            'intern_name'      => "User",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
        ]);
    }

    /**
     * @Route("/admin/User/list/", name="UserList")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request) {



        $username = $this->getUser();

        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);
        $state    = [];
        $pdf      = 0;
        $get      = $request->query->get('get');
        $archiv   = $request->query->get('archiv', false);
        $archiv   = trim($archiv) === "true";
        $names    = [];

        $pName       = "User";
        $pInternal   = "User";
        $repo        = $this->getDoctrine()->getRepository(User::class);
        $users       = $repo->findAllWithArchiveWithoutOrder((bool) $archiv);
        $currentUser = false;
        $ids = [];
        if (count($users) > 0) {
            $ids      = array_keys($users);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentUser = $users[$curentId];
        }

        return $this->render('user/list.twig', [
            'users'       => $users,
            'name'        => $pName,
            'id'          => $ids,
            'intern_name' => $pInternal,
            'state'       => $state,
            'user'        => $username->getDisplayName(),
            'user_role'   => $role,
            'pdf'         => $pdf,
            'names'       => $names,
            'get_var'     => $get,
            'currentUser' => $currentUser,
        ]);
    }

    /**
     * @Route("/admin/User/delete/{id}", name="WarengruppeDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $UserRepo = $this->getDoctrine()->getRepository(User::class);
        $username = $this->getUser();
        $isAdmin = $username->getRoles()[0] == "ROLE_ADMIN";
        if(! $isAdmin){
            return $this->redirectToRoute('UserList');
        }
        /**
         * @var User $User
         * @var User $newUser
         */
        $User = $UserRepo->find($id);

        if(count($UserRepo->getCountEnabled()) == 1){

            User::Remove($User);

            $manager->flush();

            return $this->redirectToRoute('UserList');
        }

        $newId    = $request->request->get("newElement", -1);
        $newUser = $UserRepo->find((int) $newId);

        $zutats = $User->getZutats();
        $liferanten = $User->getLieferants();
        $warengruppen =  $User->getCreatedwarengruppe();
        $wareneingangen =  $User->getWarenannahme();

        foreach($zutats as $zutat){
            $zutat->setUserAdded($newUser);
        }

        foreach($liferanten as $lieferant){
            $lieferant->setUserAdded($newUser);
        }

        foreach($warengruppen as $warengruppe){
            $warengruppe->setUserAdded($newUser);
        }

        foreach($wareneingangen as $wareneingang){
            $wareneingang->setUserAdded($newUser);
        }

        User::Remove($User);

        $manager->flush();

        return $this->redirectToRoute('UserList');
    }


    /**
     * @Route("/admin/User/archive/{id}", name="UserArchive")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function archive($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $UserRepo = $this->getDoctrine()->getRepository(User::class);
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        /**
         * @var User $User
         */
        $User = $UserRepo->find($id);

        User::Archive($User);

        $manager->flush();

        return $this->redirectToRoute('UserList');
    }

    /**
     * @Route("/admin/User/edit/{id}", name="UserEdit")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $item = $repo->find($id);
        $username    = $this->getUser();
        $manager     = $this->getDoctrine()->getManager();

        if(! $this->checkEditAccess($username, $item)) {
            return $this->redirectToRoute('UserList');
        }
        $manager->flush();



        /**
         * module for all controllers
         */
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }
        /**
         *
         */

        $form = $this->createForm(UserFormType::class, $item);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setPassword($passwordEncoder->encodePassword($item, "thesecurityisonlybullshit"));
            $item->makeAdmin();
            $item->makeInventoryAuthorized();


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "UserList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('user/edit.twig', [
            'form'             => $form->createView(),
            'name'             => "User",
            'item'             => "User",
            'intern_name'      => "User",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'id'               => $id,
        ]);
    }


    /**
     * @Route("/admin/User/edit_check", name="UserEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateCheckForRequest($request, User::class);
    }

    /**
     * @Route("/admin/User/edit_update", name="UserEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateUpdateForRequest($request, User::class);
    }
}