<?php

namespace App\Controller\Traits;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Lieferant;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

trait EditTrait {


    protected function checkEditAccess($user, $item, $setData = true) {
        /**
         * @var AbstractEntity $item
         * @var User           $user
         */
        $result = false;
        if (! $item->getEditorId() || $item->getEditorId() == $user->getId()) {
            $result = true;
        }

        if (! $item->getDateEdit() || $item->getDateEdit()->getTimestamp() + 30 < time()) {
            $result = true;
        }

        if ($result && $setData) {
            $item->setDateEdit(new \DateTime('now'));
            $item->setEditorId($user->getId());
        }

        return $result;
    }

    protected function editStateCheckForRequest($request, $entityClass) {
        $user = $this->getUser();
        $ids  = $request->query->get("ids", "[]");
        $ids  = json_decode($ids, true);
        $data = [];

        $Repo = $this->getDoctrine()->getRepository($entityClass);

        foreach ($ids as $id) {
            $entity = $Repo->find((int) $id);
            /**
             * @var AbstractEntity $entity
             */
            $data[$entity->getId()] = true;

            if ($entity->getEditorId() && $entity->getDateEdit()) {

                if (
                    $user->getId() !== $entity->getEditorId() &&
                    $entity->getDateEdit()->getTimestamp() + 30 > time()
                ) {
                    $data[$entity->getId()] = false;
                }
            }
        }

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    protected function editStateUpdateForRequest($request, $entityClass) {
        $manager = $this->getDoctrine()->getManager();
        $user    = $this->getUser();
        $id      = $request->query->get("id", -1);
        $Repo    = $this->getDoctrine()->getRepository($entityClass);
        $entity  = $Repo->find((int) $id);
        /**
         * @var AbstractEntity $entity
         */
        if ($entity) {
            if ($entity->getEditorId() == $user->getId()) {
                $entity->setDateEdit(new \DateTime('now'));
            }
        }
        $manager->flush();

        return new Response();
    }
}