<?php

namespace App\Controller\Traits;

use App\Entity\AbstractClasses\AbstractEntity;
use App\Entity\Lieferant;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

trait AuthorizationTrait {



    protected function checkListAccess($user, $roles) {
        /**
         * @var AbstractEntity $item
         * @var User           $user
         */
        if(!is_array($roles)){
            $roles = [$roles];
        }
        foreach($roles as $role){
            if(!$this->startsWith($role, "ROLE_")){
                $role = "ROLE_" . $role;
            }
            if(in_array($role, $user->getRoles())){
                return true;
            }
        }


        return false;
    }

    protected function generateRoleCode($user){
        $role = 0;
        if (in_array("ROLE_ADMIN", $user->getRoles())) {
            $role = $role + 10;
        }
        if (in_array("ROLE_INVENTORY", $user->getRoles())) {
            $role = $role + 1;
        }
        return $role;
    }

    private function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }




}