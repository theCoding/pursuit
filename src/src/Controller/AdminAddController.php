<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Warengruppe;
use App\Entity\Lieferant;
use App\Entity\User;
use App\Entity\Zutat;
use App\Entity\Wareneingang;
use App\Entity\Rezeptur;
use App\Entity\Ansatz;
use App\Entity\Produkt;
use App\Entity\Abfullung;
use App\Entity\Einheit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminAddController extends AbstractController
{
    /**
     * @Route("/admin/add", name="admin_add")
     */
    public function index()
    {
        return $this->render('admin_add/index.html.twig', [
            'controller_name' => 'AdminAddController',
        ]);
    }

    /**
     * @Route("/admin/add/{pItem}", name="add")
     */
    public function add($pItem, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $username = $this->getUser();
        $role = 0;
        if (in_array("ROLE_ADMIN", $username->getRoles())) {
            $role = $role + 1;
        }
        if (in_array("ROLE_INVENTORY", $username->getRoles())) {
            $role = $role + 10;
        }

        $object_id = "";
        $e1effect = "";
        $ZutatString = "";
        $e1is = "";
        $ae1 = [];
        $ae2 = [];
        $lieferanten = "";
        $zutaten = "";
        $repository = $this->getDoctrine()->getRepository(User::class);
        $manager = $this->getDoctrine()->getManager();
        $class = new \ReflectionClass("App\Entity\\" . $pItem);
        $item = $class->newInstanceArgs();
        if ($pItem == "Lieferant") {


            return $this->redirectToRoute("LieferantAddPage");

        } else if ($pItem == "User") {

            return $this->redirectToRoute("UserAddPage");

        } else if ($pItem == "Zutat") {

            return $this->redirectToRoute("ZutatAddPage");

        } else if ($pItem == "Warengruppe") {

            return $this->redirectToRoute("WarengruppeAddPage");

        } else if ($pItem == "Wareneingang") {

            return $this->redirectToRoute("WareneingangAddPage");

        } else if ($pItem == "Rezeptur") {
            $lieferantrepo = $this->getDoctrine()->getRepository(Lieferant::class);
            $lieferanten = $lieferantrepo->findAll();


            $zutatenrepo = $this->getDoctrine()->getRepository(Zutat::class);
            $zutaten = $zutatenrepo->findAll();


            $lieferanten = $lieferantrepo->findAll();
            $zutaten = $zutatenrepo->findAll();
            $form = $this->createForm('App\Form\\' . $pItem . 'FormType'::class, $item);
        } else if ($pItem == "Produkt") {
            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte = $rezeptrepo->findAll();
            $rezept_array = [];
            foreach ($rezepte as $key => $rezept) {

                $rezept_array = $rezept_array + [$rezept->getBezeichnung() => $key];
            }
            $form_options = ['rezepte' => $rezept_array];
            $form = $this->createForm('App\Form\\' . $pItem . 'FormType'::class, $item, $form_options);
        } else if ($pItem == "Ansatz") {
            $rezeptrepo = $this->getDoctrine()->getRepository(Rezeptur::class);
            $rezepte = $rezeptrepo->findAll();
            $rezept_array = [];
            foreach ($rezepte as $key => $rezept) {

                $rezept_array = $rezept_array + [$rezept->getBezeichnung() => $key];
            }
            $form_options = ['rezept' => $rezept_array];
            $form = $this->createForm('App\Form\\' . $pItem . 'FormType'::class, $item, $form_options);
        } else if ($pItem == "Abfullung") {
            $produktrepo = $this->getDoctrine()->getRepository(Produkt::class);
            $produkte = $produktrepo->findAll();
            $produkt_array = [];
            foreach ($produkte as $key => $produkt) {

                $produkt_array = $produkt_array + [$produkt->getBezeichnung() => $key];
            }

            $ansatzrepo = $this->getDoctrine()->getRepository(Ansatz::class);
            $ansatze = $ansatzrepo->findAll();
            $ansatz_array = [];
            foreach ($ansatze as $key => $ansatz) {

                $ansatz_array = $ansatz_array + [$ansatz->getRezept()->getBezeichnung() => $key];
            }

            $form_options = ['produkt' => $produkt_array, 'ansatz' => $ansatz_array];
            $form = $this->createForm('App\Form\\' . $pItem . 'FormType'::class, $item, $form_options);
        } else if ($pItem == "Einheit") {

            return $this->redirectToRoute("EinheitAddPage");

        }


        $form->handleRequest($request);

        $save = false;
        $permissions = true;
        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            if ($pItem == "User") {
                $item->setPassword($passwordEncoder->encodePassword($item, "thesecurityisonlybullshit"));
                $item->makeAdmin();
                $item->makeInventoryAuthorized();
            } else {


                $item->setDateAdded(new \DateTime('now'));

                $item->setUserAdded($username);
                $item->setObjectState(2);
                if ($pItem == "Lieferant") {

                } else if ($pItem == "Zutat") {
//                    $item->makeLieferInt($lieferanten);
//                    $item->makeGruppeInt($warengruppen);
//                    $item->setMenge(0);

                } else if ($pItem == "Wareneingang") {
//
//
//                    if ($item->getInventur() == 1) {
//                        if ($role > 0 && $role < 100) {
//                            $item->makeZutatInt($zutaten);
//                            $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
//                            $item->setCharge("00");
//                            $item->setLieferschein("INV");
//                            $zutat->setMenge($item->getMenge());
//                        } else {
//                            $permissions = false;
//                        }
//
//
//                    } else {
//                        $item->makeLieferInt($lieferanten);
//                        $item->makeZutatInt($zutaten);
//                        $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
//                        $zutat->setMenge($zutat->getMenge() + $item->getMenge());
//                    }

                } else if ($pItem == "Warengruppe") {
                    //dump($_POST);
//                    $item->makeGruppeInt($warengruppen);

                } else if ($pItem == "Rezeptur") {
                    //dump($item);
                    $item->makeZutatString($zutaten, $lieferanten);
                    //dump($item);
                    $lieferanten = $lieferantrepo->findAll();
                    $zutaten = $zutatenrepo->findAll();

                } else if ($pItem == "Produkt") {
                    $item->makeRezeptInt($rezepte);
                } else if ($pItem == "Ansatz") {
                    $item->makeProduktInt($rezepte);
                    $zutaten = $item->getRezept()->getZutaten();
                    $mengen = $item->getRezept()->getMenge();
                    for ($i = 0; $i < sizeof($zutaten); $i++) {
                        $zutaten[$i]->setMenge($zutaten[$i]->getMenge() - $mengen[$i]);
                        //dump($zutaten[$i]);
                    }
                } else if ($pItem == "Abfullung") {
                    $item->makeAnsatzInt($ansatze);

                    $item->makeProduktInt($produkte);
                    //dump($item);
                }

            }


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();
                $save = true;
            }


        }


        return $this->render('add_template.html.twig', [
            'form' => $form->createView(),
            'name' => $pItem,
            'item' => $pItem,
            'intern_name' => $pItem,
            'saved' => $save,
            'lieferanten' => $lieferanten,
            'zutaten' => $zutaten,
            'ausschluss_e1' => $ae1,
            'ausschluss_e2' => $ae2,
            'e1is' => $e1is,
            'e1effect' => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id' => $object_id,
            'user' => $username->getDisplayName(),
            'user_role' => $role,
            'pdf' => '',
        ]);

        //return $this->render('adminadd.html.twig');

    }
}
