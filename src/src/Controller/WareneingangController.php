<?php

namespace App\Controller;

use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Abfullung;
use App\Entity\Ansatz;
use App\Entity\Einheit;
use App\Entity\Lieferant;
use App\Entity\Produkt;
use App\Entity\User;
use App\Entity\Wareneingang;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\AbfullungFormType;
use App\Form\LieferantFormType;
use App\Form\WareneingangFormType;
use App\Form\ZutatFormType;
use App\Repository\WareneingangRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WareneingangController extends AbstractController {

    use EditTrait;
    use AuthorizationTrait;

    /**
     * @Route("/admin/Lieferant/add", name="LieferantAdd")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        /**
         * module for all controllers
         */
        $username = $this->getUser();
        if (! $this->checkListAccess($username, "USER")) {
            return $this->redirectToRoute('Dashboard');
        }
        $manager     = $this->getDoctrine()->getManager();
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role        = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->getAllForHerstellerOrLieferant(Lieferant::IS_LIEFERANT);
        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }

        $zutatenrepo   = $this->getDoctrine()->getRepository(Zutat::class);
        $zutaten       = $zutatenrepo->findAll();
        $zutaten_array = [];

        foreach ($zutaten as $key => $zutat) {
            if ($zutat->getObjectState() != 3) {
                $zutaten_array = $zutaten_array + [$zutat->getName() => $zutat];
            }
        }

        foreach ($zutaten as $zutat) {
            $lieferanten_zutat_array = [];
            foreach ($zutat->getLieferant() as $lieferant) {
                array_push($lieferanten_zutat_array, $lieferant->getAlias());
            }
            $ae1 = $ae1 + [$zutat->getName() => $lieferanten_zutat_array];
        }

        $form_options = ['lieferanten' => $lieferanten_array, 'zutaten' => $zutaten_array];
        $ae1          = [];

        foreach ($zutaten as $zutat) {
            $ae1 = $ae1 + [$zutat->getName() => $zutat->getLieferant()->getSnapshot()];
        }

        $item = new Wareneingang();

        $form = $this->createForm(WareneingangFormType::class, $item, $form_options);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(2);


            /**
             *  Handler
             */
            if ($item->getInventur() == 1) {
                if ($role > 0 && $role < 100) {
                    $item->makeZutatInt($zutaten);
                    $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
                    $item->setCharge("00");
                    $item->setLieferschein("INV");
                    $zutat->setMenge($item->getMenge());
                } else {
                    $permissions = false;
                }
            } else {
                $item->makeLieferInt($lieferanten);
                $item->makeZutatInt($zutaten);
                $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
                $zutat->setMenge($zutat->getMenge() + $item->getMenge());
            }


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "WareneingangList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('wareneingang/add.twig', [
            'form'             => $form->createView(),
            'name'             => "Wareneingang",
            'item'             => "Wareneingang",
            'intern_name'      => "Wareneingang",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
        ]);
    }


    //"Zutat", "Charge", "Lieferschein", "Qualitätscheck", "Menge", "Lieferant", "Hinzugefüg am", "Angelegt von"


    /**
     * @Route("/admin/Wareneingang/list/", name="WareneingangList")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request) {
        $username = $this->getUser();
        if (! $this->checkListAccess($username, "USER")) {
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);

        $state  = [];
        $archiv = $request->query->get('archiv', false);
        $archiv = trim($archiv) === "true";
        $pdf    = 0;
        $get    = $request->query->get('get');
        $names  = [];

        $pName               = "Wareneingang";
        $pInternal           = "Wareneingang";
        $repo                = $this->getDoctrine()->getRepository(Wareneingang::class);
        $wareneingangen      = $repo->findAllWithArchive((bool) $archiv);
        $currentWareneingang = false;
        $ids                 = [];
        if (count($wareneingangen) > 0) {
            $ids      = array_keys($wareneingangen);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentWareneingang = $wareneingangen[$curentId];
        }

        return $this->render('wareneingang/list.twig', [
            'wareneingangen'      => $wareneingangen,
            'name'                => $pName,
            'id'                  => $ids,
            'intern_name'         => $pInternal,
            'state'               => $state,
            'user'                => $username->getDisplayName(),
            'user_role'           => $role,
            'pdf'                 => $pdf,
            'names'               => $names,
            'get_var'             => $get,
            'currentWareneingang' => $currentWareneingang,
        ]);
    }


    /**
     * @Route("/admin/Wareneingang/delete/{id}", name="WareneingangDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $manager          = $this->getDoctrine()->getManager();
        $WareneingangRepo = $this->getDoctrine()->getRepository(Wareneingang::class);

        $username = $this->getUser();
        $isAdmin  = $username->getRoles()[0] == "ROLE_ADMIN";
        if (! $isAdmin) {
            return $this->redirectToRoute('WareneingangList');
        }

        /**
         * @var Wareneingang $wareneingang
         * @var Wareneingang $newWareningang
         */
        $wareneingang = $WareneingangRepo->find($id);

        Wareneingang::Remove($wareneingang);

        $manager->flush();

        return $this->redirectToRoute('WareneingangList');
    }

    /**
     * @Route("/admin/Wareneingang/archive/{id}", name="WareneingangArchive")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function archive($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $manager          = $this->getDoctrine()->getManager();
        $WareneingangRepo = $this->getDoctrine()->getRepository(Wareneingang::class);
        $username         = $this->getUser();
        if (! $this->checkListAccess($username, "ADMIN")) {
            return $this->redirectToRoute('Dashboard');
        }
        /**
         * @var Wareneingang $Wareneingang
         */
        $Wareneingang = $WareneingangRepo->find($id);

        Zutat::Archive($Wareneingang);

        $manager->flush();

        return $this->redirectToRoute('WareneingangList');
    }

    /**
     * @Route("/admin/Wareneingang/edit/{id}", name="WareneingangEdit")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $repo     = $this->getDoctrine()->getRepository(Wareneingang::class);
        $item     = $repo->find($id);
        $username = $this->getUser();
        $manager  = $this->getDoctrine()->getManager();

        if (! $this->checkEditAccess($username, $item)) {
            return $this->redirectToRoute('WareneingangList');
        }
        $manager->flush();
        /**
         * module for all controllers
         */

        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role        = 0;
        $role        = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }


        /**
         *  Form create
         */
        $lieferantrepo     = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->getAllForHerstellerOrLieferant(Lieferant::IS_LIEFERANT);

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array = $lieferanten_array + [$lieferant->getAlias() => $key];
            }
        }


        $zutatenrepo   = $this->getDoctrine()->getRepository(Zutat::class);
        $zutaten       = $zutatenrepo->findAll();
        $zutaten_array = [];

        foreach ($zutaten as $key => $zutat) {
            if ($zutat->getObjectState() != 3) {
                $zutaten_array = $zutaten_array + [$zutat->getName() => $zutat];
            }
        }

        foreach ($zutaten as $zutat) {
            $lieferanten_zutat_array = [];
            foreach ($zutat->getLieferant() as $lieferant) {
                array_push($lieferanten_zutat_array, $lieferant->getAlias());
            }
            $ae1 = $ae1 + [$zutat->getName() => $lieferanten_zutat_array];
        }

        $form_options = ['lieferanten' => $lieferanten_array, 'zutaten' => $zutaten_array];
        $ae1          = [];

        foreach ($zutaten as $zutat) {
            $ae1 = $ae1 + [$zutat->getName() => $zutat->getLieferant()->getSnapshot()];
        }

        $repo      = $this->getDoctrine()->getRepository(Wareneingang::class);
        $item      = $repo->find($id);
        $old_menge = $item->getMenge();
        $form      = $this->createForm(WareneingangFormType::class, $item, $form_options);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();


            /**
             *  Handler
             */
            //if ($item->getInventur() == 1) {
            /* if ($role > 0 && $role < 100) {
                 $item->makeZutatInt($zutaten);
                 $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
                 $item->setCharge("00");
                 $item->setLieferschein("INV");
                 $zutat->setMenge($item->getMenge());
             } else {
                 $permissions = false;
             }
*/

            //  } else {
            $item->makeLieferInt($lieferanten);
            $item->makeZutatInt($zutaten);
            $zutat = $zutatenrepo->findOneBy(['Name' => $item->getZutat()->getName()]);
            $zutat->setMenge($zutat->getMenge() - $old_menge + $item->getMenge());
            //}


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "WareneingangList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('wareneingang/edit.twig', [
            'form'             => $form->createView(),
            'name'             => "Wareneingang",
            'item'             => "Wareneingang",
            'intern_name'      => "Wareneingang",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'id'               => $id,
        ]);
    }

    /**
     * @Route("/admin/Wareneingang/edit_check", name="WareneingangEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        return $this->editStateCheckForRequest($request, Wareneingang::class);
    }

    /**
     * @Route("/admin/Wareneingang/edit_update", name="WareneingangEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        return $this->editStateUpdateForRequest($request, Wareneingang::class);
    }


    private function validateDate($date, $format = 'd.m.Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
    /**
     * @Route("/admin/Wareneingang/print", name="WareneingangPrint")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function print(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $dateStart = $request->query->get("dateStart", -1);
        $dateStop  = $request->query->get("dateStop", -1);
        if (! $dateStart || ! $dateStop ||   ! $this->validateDate($dateStart) || !$this->validateDate($dateStart)) {
            echo "Datumsbereich nicht eingestellt!";
            exit;
        }
        $dateStart = \DateTime::createFromFormat('d.m.Y', $dateStart)->format("Y-m-d");
        $dateStop = \DateTime::createFromFormat('d.m.Y', $dateStop)->add(date_interval_create_from_date_string('24 hours'))->format("Y-m-d");

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');


        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file

        $pName         = "Wareneingang";
        $pInternal     = "Wareneingang";
        $eingangrepo   = $this->getDoctrine()->getRepository(Wareneingang::class);
        /**
         * @var WareneingangRepository $eingangrepo
         */
        $wareneingange = $eingangrepo->getAllForDateRange($dateStart, $dateStop);
        if(count($wareneingange)==0){
            echo "Für den ausgewählten Zeitraum liegen keine Wareneingan vor";
            exit;
        }

        $wareneingangePages = array_chunk($wareneingange, 5, true);
        $pagesCount = count($wareneingangePages);


        $link       = ["", "", "", "", "", "", ""];
        $info_array = [];
        $ids        = [];

        $html = $this->renderView('pdf/Wareneingang.html.twig', [
            'pagesCount'     => $pagesCount,
            'wareneingangePages' => $wareneingangePages,
        ]);


        $dompdf->set_option('dpi', '200');

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("Wareneingang($dateStart - $dateStop).pdf",[
            "Attachment" => false
        ]);

        return new Response(
            '<script>
        window.location = "/panel/Wareneingang";
        </script>'
        );
    }
}