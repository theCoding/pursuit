<?php

namespace App\Controller;

use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Abfullung;
use App\Entity\Ansatz;
use App\Entity\Einheit;
use App\Entity\Lieferant;
use App\Entity\Produkt;
use App\Entity\User;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\AbfullungFormType;
use App\Form\LieferantFormType;
use App\Form\ZutatFormType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LieferantController extends AbstractController
{

    use EditTrait;
    use AuthorizationTrait;
    /**
     * @Route("/admin/Lieferant/add", name="LieferantAdd")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        /**
         * module for all controllers
         */
        $username = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $manager = $this->getDoctrine()->getManager();
        $zutaten = "";
        $ae1 = [];
        $ae2 = [];
        $object_id = "";
        $e1effect = "";
        $ZutatString = "";
        $e1is = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }
        /**
         *
         */
        $item = new Lieferant();
        $form = $this->createForm(LieferantFormType::class, $item);

        $form->handleRequest($request);

        $save = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(2);


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "LieferantList",
                    [
                        "get" => $item->getId()
                    ]
                );
            }
        }


        return $this->render('lieferant/add.twig', [
            'form' => $form->createView(),
            'name' => "Lieferant",
            'item' => "Lieferant",
            'intern_name' => "Lieferant",
            'saved' => $save,
            'lieferanten' => $lieferanten,
            'zutaten' => $zutaten,
            'ausschluss_e1' => $ae1,
            'ausschluss_e2' => $ae2,
            'e1is' => $e1is,
            'e1effect' => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id' => $object_id,
            'user' => $username->getDisplayName(),
            'user_role' => $role,
            'pdf' => '',
        ]);

    }

    /**
     * @Route("/admin/Lieverant/list/", name="EinheitList")
     * @param Request $request
     *
     * @return Response
     */
    public function list(Request $request) {
        $username = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);

        $state    = [];
        $archiv   = $request->query->get('archiv', false);
        $archiv   = trim($archiv) === "true";
        $pdf      = 0;
        $get      = $request->query->get('get');
        $names    = [];

        $pName       = "Lieferant";
        $pInternal   = "Lieferant";
        $repo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $repo->findAllWithArchive((bool) $archiv);

        $currentLieferant = false;
        $ids = [];
        if (count($lieferanten) > 0) {
            $ids      = array_keys($lieferanten);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentLieferant = $lieferanten[$curentId];
        }

        return $this->render('lieferant/list.twig', [
            'lieferanten'   => $lieferanten,
            'name'        => $pName,
            'id'          => $ids,
            'intern_name' => $pInternal,
            'state'       => $state,
            'user'        => $username->getDisplayName(),
            'user_role'   => $role,
            'pdf'         => $pdf,
            'names'       => $names,
            'get_var'     => $get,
            'currentLieferant'   => $currentLieferant,
        ]);
    }


    /**
     * @Route("/admin/Lieferant/delete/{id}", name="LieferantDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $LieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $isAdmin = $username->getRoles()[0] == "ROLE_ADMIN" ;
        if(! $isAdmin){
            return $this->redirectToRoute('LieferantList');
        }
        /**
         * @var Lieferant $lieferant
         * @var Lieferant $newLieferant
         */
        $lieferant = $LieferantRepo->find($id);

        if(count($LieferantRepo->getCountEnabled()) == 1){
            Lieferant::Remove($lieferant);

            $manager->flush();

            return $this->redirectToRoute('LieferantList');
        }

        $newId    = $request->request->get("newElement", -1);
        $newLieferant = $LieferantRepo->find((int) $newId);
        if (! $newLieferant) {
            throw new Exception("New Lieferant not found");
        }

        $Wareneingangs = $lieferant->getWareneingangs();
        $Zutats = $lieferant->getZutats();

        foreach ($Wareneingangs as $Wareneingang) {
            $Wareneingang->setLieferant($newLieferant);
        }
        foreach ($Zutats as $zutat) {
            $zutat->removeLieferant($lieferant);
            $zutat->removeLieferant($newLieferant);
        }


        Lieferant::Remove($lieferant);

        $manager->flush();

        return $this->redirectToRoute('LieferantList');
    }


    /**
     * @Route("/admin/Lieferant/archive/{id}", name="LieferantArchive")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws Exception
     */
    public function archive($id, Request $request){
        $manager   = $this->getDoctrine()->getManager();
        $LieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        /**
         * @var Lieferant $Lieferant
         */
        $Lieferant = $LieferantRepo->find($id);

        Lieferant::Archive($Lieferant);

        $manager->flush();

        return $this->redirectToRoute('LieferantList');
    }

    /**
     * @Route("/admin/Lieferant/edit/{id}", name="LieferantEdit")
     * @param                              $id
     * @param Request                      $request
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     * @throws Exception
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $username = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Lieferant::class);
        $item = $repo->find($id);
        $manager = $this->getDoctrine()->getManager();
        if(! $this->checkEditAccess($username, $item)){
            return $this->redirectToRoute('LieferantList');
        }
        $manager->flush();
        /**
         * module for all controllers
         */


        $zutaten = "";
        $ae1 = [];
        $ae2 = [];
        $object_id = "";
        $e1effect = "";
        $ZutatString = "";
        $e1is = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }



        $form = $this->createForm(LieferantFormType::class, $item);

        $form->handleRequest($request);

        $save = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();


            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "LieferantList",
                    [
                        "get" => $item->getId()
                    ]
                );
            }
        }


        return $this->render('lieferant/edit.twig', [
            'form' => $form->createView(),
            'name' => "Lieferant",
            'item' => "Lieferant",
            'intern_name' => "Lieferant",
            'saved' => $save,
            'lieferanten' => $lieferanten,
            'zutaten' => $zutaten,
            'ausschluss_e1' => $ae1,
            'ausschluss_e2' => $ae2,
            'e1is' => $e1is,
            'e1effect' => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id' => $object_id,
            'user' => $username->getDisplayName(),
            'user_role' => $role,
            'pdf' => '',
            'id'  => $id,
        ]);

    }


    /**
     * @Route("/admin/Lieferant/edit_check", name="LieferantEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateCheckForRequest($request, Lieferant::class);
    }

    /**
     * @Route("/admin/Lieferant/edit_update", name="LieferantEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateUpdateForRequest($request, Lieferant::class);
    }


}