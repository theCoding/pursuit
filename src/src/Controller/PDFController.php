<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Entity\Wareneingang;
use Symfony\Component\HttpFoundation\Request;

class PDFController extends AbstractController
{
    /**
     * @Route("/createpdf/{pItem}", name="pdf")
     */
    public function pdf($pItem, Request $request)
    {

       // Configure Dompdf according to your needs
       $pdfOptions = new Options();
       $pdfOptions->set('defaultFont', 'Arial');
       
       
       // Instantiate Dompdf with our options
       $dompdf = new Dompdf($pdfOptions);
       
       // Retrieve the HTML generated in our twig file
       if($pItem == "Wareneingang"){

        $pName = "Wareneingang";
        $pInternal = "Wareneingang";
        $eingangrepo = $this->getDoctrine()->getRepository(Wareneingang::class);
        $wareneingange = $eingangrepo->findAll();

        $date = $request->query->get('date');
        $date_array = explode(" - ", $date);

        $start_date = explode(".", $date_array[0]);
        $start_date[0] = str_replace('"', "", $start_date[0]);
        $start_date = new \DateTime($start_date[0] . "-" . $start_date[1] . "-". $start_date[2]);
        $end_date = explode(".", $date_array[1]);
        $end_date[2] = str_replace('"', "", $end_date[2]);
        $end_date = new \DateTime($end_date[0] . "-" . $end_date[1] . "-". $end_date[2]);
       
    $headings = ["Lieferant", "Eingangsdatum", "Zutat", "Charge", "Lieferschein", "QC", "Menge", "Bearbeiter", "Unterschrift"];
    $link = ["", "", "", "", "", "", ""];
    $info_array = [];
    $ids = [];
    foreach($wareneingange as $eingang){

        $charge = $eingang->getCharge();
        $lieferschein = $eingang->getLieferschein();
        
        if(!($lieferschein == "INV" && $charge == 0)){
           
            $date = new \DateTime($eingang->getDateAdded()->format('Y-m-d'));
            if($date>=$start_date && $date<=$end_date){
                $items = [];
                array_push($items, $eingang->getLieferant()->getAlias());
                array_push($items, $eingang->getDateAdded()->format('Y-m-d'));
                array_push($items, $eingang->getZutat()->getName());
                array_push($items, $charge);
                array_push($items, $lieferschein);
                array_push($items, ($eingang->getQualitaetscheck() == 1 ? "ok" : "nicht ok"));
                array_push($items, $eingang->getMenge() . $eingang->getZutat()->getEinheit());
         
                    
                
        
        
                
                
                array_push($items, $eingang->getUserAdded()->getDisplayName());
                array_push($items, "img");
                array_push($items, $eingang->getUserAdded()->getSignature());
        
                array_push($ids, $eingang->getId());
              
                array_push($info_array, $items);
             }
           
           
            
        }
      
       
        
    }


        $html = $this->renderView('pdf/Wareneingang.html.twig', [
            'name' => $pName,
            'headings' => $headings,
            'elements' => $info_array,
            'links' => $link,
            'id' => $ids,
        ]);


       }
      
       $dompdf->set_option( 'dpi' , '200' );
       
       // Load HTML to Dompdf
       $dompdf->loadHtml($html);
       
       // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
       $dompdf->setPaper('A4', 'landscape');

       // Render the HTML as PDF
       $dompdf->render();

       // Output the generated PDF to Browser (force download)
       $dompdf->stream($pItem . ".pdf", [
           "Attachment" => true
       ]);
       return new Response(
        '<script>
        window.location = "/panel/{{ item }}";
        </script>'
    );
       
    }
}
