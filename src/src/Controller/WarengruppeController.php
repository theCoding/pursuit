<?php

namespace App\Controller;

use App\Controller\Traits\EditTrait;
use App\Controller\Traits\AuthorizationTrait;
use App\Entity\Abfullung;
use App\Entity\Ansatz;
use App\Entity\Einheit;
use App\Entity\Lieferant;
use App\Entity\Produkt;
use App\Entity\User;
use App\Entity\Wareneingang;
use App\Entity\Warengruppe;
use App\Entity\Zutat;
use App\Form\AbfullungFormType;
use App\Form\LieferantFormType;
use App\Form\WarengruppeFormType;
use App\Form\ZutatFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WarengruppeController extends AbstractController {

    use EditTrait;
    use AuthorizationTrait;

    /**
     * @Route("/admin/Lieferant/add", name="LieferantAdd")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        /**
         * module for all controllers
         */
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, ["ADMIN", "INVENTORY"])){
            return $this->redirectToRoute('Dashboard');
        }
        $manager     = $this->getDoctrine()->getManager();
        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }
        /**
         *
         */


        $item = new Warengruppe();

        $warengrupperepo   = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppen      = $warengrupperepo->findAll();
        $warengruppe_array = [];
        foreach ($warengruppen as $key => $warengruppe) {
            if ($warengruppe->getObjectState() != 3) {
                $warengruppe_array = $warengruppe_array + [$warengruppe->getName() => $key];
            }
        }

        $warengruppe_array = $warengruppe_array + ["-" => -1];
        $form_options      = ['warengruppe' => $warengruppe_array];

        $form = $this->createForm(WarengruppeFormType::class, $item, $form_options);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->setDateAdded(new \DateTime('now'));
            $item->setUserAdded($username);
            $item->setObjectState(2);
            $item->makeGruppeInt($warengruppen);

            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "WarengruppeList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('warengruppe/add.twig', [
            'form'             => $form->createView(),
            'name'             => "Warengruppe",
            'item'             => "Warengruppe",
            'intern_name'      => "Warengruppe",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
        ]);
    }

    /**
     * @Route("/admin/Warengruppe/list/", name="WarengruppeList")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request) {
        $username = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        $role = $this->generateRoleCode($username);

        $state    = [];
        $archiv   = $request->query->get('archiv', false);
        $archiv   = trim($archiv) === "true";
        $pdf      = 0;
        $get      = $request->query->get('get');
        $names    = [];

        $pName        = "Warengruppe";
        $pInternal    = "Warengruppe";
        $repo         = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppen = $repo->findAllWithArchive((bool) $archiv);
        $currentWarengruppe = false;
        $ids = [];
        if (count($warengruppen) > 0) {
            $ids      = array_keys($warengruppen);
            $curentId = $ids[0];

            if (in_array((int) $get, $ids)) {
                $curentId = (int) $get;
            }

            $currentWarengruppe = $warengruppen[$curentId];
        }

        return $this->render('warengruppe/list.twig', [
            'warengruppe'        => $warengruppen,
            'name'               => $pName,
            'id'                 => $ids,
            'intern_name'        => $pInternal,
            'state'              => $state,
            'user'               => $username->getDisplayName(),
            'user_role'          => $role,
            'pdf'                => $pdf,
            'names'              => $names,
            'get_var'            => $get,
            'currentWarengruppe' => $currentWarengruppe,
        ]);
    }

    /**
     * @Route("/admin/Warengruppe/delete/{id}", name="WarengruppeDelete")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $manager   = $this->getDoctrine()->getManager();
        $WarengruppeRepo = $this->getDoctrine()->getRepository(Warengruppe::class);

        $username = $this->getUser();
        $isAdmin = $username->getRoles()[0] == "ROLE_ADMIN" ;
        if(! $isAdmin){
            return $this->redirectToRoute('WarengruppeList');
        }

        /**
         * @var Warengruppe $wareneingang
         * @var Warengruppe $newWareningang
         */
        $Warengruppe = $WarengruppeRepo->find($id);

        Warengruppe::Remove($Warengruppe);

        $manager->flush();

        return $this->redirectToRoute('WarengruppeList');
    }

    /**
     * @Route("/admin/Warengruppe/archive/{id}", name="WarengruppeArchive")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function archive($id, Request $request, UserPasswordEncoderInterface $passwordEncoder){

        $manager   = $this->getDoctrine()->getManager();
        $WarengruppeRepo = $this->getDoctrine()->getRepository(Warengruppe::class);
        $username    = $this->getUser();
        if(! $this->checkListAccess($username, "ADMIN")){
            return $this->redirectToRoute('Dashboard');
        }
        /**
         * @var Warengruppe $Warengruppe
         */
        $Warengruppe = $WarengruppeRepo->find($id);

        Warengruppe::Archive($Warengruppe);

        $manager->flush();

        return $this->redirectToRoute('WarengruppeList');
    }


    /**
     * @Route("/admin/Warengruppe/edit/{id}", name="WarengruppeEdit")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $repo = $this->getDoctrine()->getRepository(Warengruppe::class);
        $item = $repo->find($id);
        $username    = $this->getUser();
        $manager     = $this->getDoctrine()->getManager();
        if(! $this->checkEditAccess($username, $item)){
            return $this->redirectToRoute('WarengruppeList');
        }
        $manager->flush();
        /**
         * module for all controllers
         */


        $zutaten     = "";
        $ae1         = [];
        $ae2         = [];
        $object_id   = "";
        $e1effect    = "";
        $ZutatString = "";
        $e1is        = "";
        $role = $this->generateRoleCode($username);

        $lieferantRepo = $this->getDoctrine()->getRepository(Lieferant::class);
        $lieferanten   = $lieferantRepo->findAll();

        $lieferanten_array = [];
        foreach ($lieferanten as $key => $lieferant) {
            if ($lieferant->getObjectState() != 3) {
                $lieferanten_array[$lieferant->getAlias()] = $key;
            }
        }
        /**
         *
         */




        $warengrupperepo   = $this->getDoctrine()->getRepository(Warengruppe::class);
        $warengruppen      = $warengrupperepo->findAll();
        $warengruppe_array = [];
        foreach ($warengruppen as $key => $warengruppe) {
            if ($warengruppe->getObjectState() != 3 && $warengruppe->getId() != $id) {
                $warengruppe_array = $warengruppe_array + [$warengruppe->getName() => $key];
            }
        }

        $warengruppe_array = $warengruppe_array + ["-" => -1];
        $form_options      = ['warengruppe' => $warengruppe_array];

        $form = $this->createForm(WarengruppeFormType::class, $item, $form_options);

        $form->handleRequest($request);

        $save        = false;
        $permissions = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $item->makeGruppeInt($warengruppen);

            if ($permissions) {
                $manager->persist($item);
                $manager->flush();

                return $this->redirectToRoute(
                    "WarengruppeList",
                    [
                        "get" => $item->getId(),
                    ]
                );
            }
        }


        return $this->render('warengruppe/edit.twig', [
            'form'             => $form->createView(),
            'name'             => "Warengruppe",
            'item'             => "Warengruppe",
            'intern_name'      => "Warengruppe",
            'saved'            => $save,
            'lieferanten'      => $lieferanten,
            'zutaten'          => $zutaten,
            'ausschluss_e1'    => $ae1,
            'ausschluss_e2'    => $ae2,
            'e1is'             => $e1is,
            'e1effect'         => $e1effect,
            'old_zutat_string' => $ZutatString,
            'object_id'        => $object_id,
            'user'             => $username->getDisplayName(),
            'user_role'        => $role,
            'pdf'              => '',
            'id'               => $id,
        ]);
    }

    /**
     * @Route("/admin/Warengruppe/edit_check}", name="WarengruppeEditCheck")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateCheck(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateCheckForRequest($request, Warengruppe::class);
    }

    /**
     * @Route("/admin/Warengruppe/edit_update", name="WarengruppeEditUpdate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editStateUpdate(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        return $this->editStateUpdateForRequest($request, Warengruppe::class);
    }
}